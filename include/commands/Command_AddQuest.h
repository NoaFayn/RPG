#ifndef COMMAND_ADDQUEST_H_INCLUDED
#define COMMAND_ADDQUEST_H_INCLUDED

#include "Command.h"

//class Game;
class Quest;

class Command_AddQuest : public Command
{
public:
	Command_AddQuest(Quest* quest);

	void start() override;

private:
	Quest* mQuest;
};

#endif // COMMAND_ADDQUEST_H_INCLUDED
