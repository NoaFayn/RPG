#ifndef SCRIPTPROCESSOR_H_INCLUDED
#define SCRIPTPROCESSOR_H_INCLUDED

#include <list>

#include "Command.h"

class ScriptProcessor
{
public:
	ScriptProcessor();

	bool hasUserControl();

	void addCommand(Command* cmd);
	void processCommands(float elapsedTime);

	void terminateCommand();

private:
	std::list<Command*> mCommands;

	bool mUserControlEnable;
};

#endif // SCRIPTPROCESSOR_H_INCLUDED
