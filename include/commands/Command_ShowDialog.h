#ifndef COMMAND_SHOWDIALOG_H_INCLUDED
#define COMMAND_SHOWDIALOG_H_INCLUDED

#include "Command.h"

class Command_ShowDialog : public Command
{
public:
	Command_ShowDialog(std::vector<std::string> lines);

	void start() override;

private:
	std::vector<std::string> mLines;
};

#endif // COMMAND_SHOWDIALOG_H_INCLUDED
