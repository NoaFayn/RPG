#ifndef COMMAND_MOVETO_H_INCLUDED
#define COMMAND_MOVETO_H_INCLUDED

#include "commands/Command.h"
#include "objects/Dynamic.h"

class Command_MoveTo : public Command
{
public:
	Command_MoveTo(Dynamic* object, float x, float y, float duration = 0.0f);

	void start() override;
	void update(float elapsedTime) override;

private:
	Dynamic* mObject;

	float mStartPosX, mStartPosY;
	float mTargetPosX, mTargetPosY;
	float mDuration;
	float mTimeSoFar;
};

#endif // COMMAND_MOVETO_H_INCLUDED
