#ifndef COMMAND_H_INCLUDED
#define COMMAND_H_INCLUDED

#include <string>
#include <vector>


class Game;


class Command
{
public:
	Command();
	virtual ~Command();

	static void initialize(Game* gameEngine);

	virtual void start() {}
	virtual void update(float elapsedTime) {}

	bool isTerminated();
	bool isStarted();

	void terminate();

protected:
	static Game* gGameEngine;

	bool mTerminated;
	bool mStarted;
};

#endif // COMMAND_H_INCLUDED
