#ifndef COMMAND_CHANGEMAP_H_INCLUDED
#define COMMAND_CHANGEMAP_H_INCLUDED

#include "Command.h"

class Command_ChangeMap : public Command
{
public:
    Command_ChangeMap(std::string mapName, float spawnX, float spawnY);

    void start() override;

private:
	std::string mMapName;

	float mDestPosX;
	float mDestPosY;
};

#endif // COMMAND_CHANGEMAP_H_INCLUDED
