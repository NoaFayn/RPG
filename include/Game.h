#ifndef GAME_H_INCLUDED
#define GAME_H_INCLUDED

#include <iostream> //std::cout | std::endl
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <vector> //std::vector
#include <string> //std::string
#include <list> //std::list
#include <algorithm> //std::remove_if

#include "constants.h"

#include "usefullPack/TextureManager.h"
#include "rpg/Map.h"
#include "objects/Creature.h"
#include "objects/Creatures/Creature_Player.hpp"
#include "rpg/Quests/Quest_Main.h"

class Quest;
class Command;


class Game
{
public:
	static const int SCREEN_WIDTH = 1000;
	static const int SCREEN_HEIGHT = 640;
	static const int SIZE_TILE = 64;

	Game();
	~Game();

	bool initialize(SDL_Window* window, SDL_Renderer* renderer);

	bool isRunning();

	void update(float ellapsedTime);
	void updateTitleScreen(float elapsedTime);
	void updateLocalMap(float elapsedTime);
	void updateWorldMap(float elapsedTime);
	void updateInventory(float elapsedTime);
	void updateShop(float elapsedTime);

	//RPG
	void changeMap(std::string mapName, float x, float y);
	void addQuest(Quest* quest);
	void showDialog(std::vector<std::string> lines);
	void summon(Dynamic* entity);

	void restart();

private:
	void displayDialog(int x, int y);


private:
	bool mRunning;
	SDL_Renderer* mRenderer;

	//Dialog infos
	std::vector<std::string> mDialogLinesToShow;
	bool mShowDialog;
	float mDialogPosX, mDialogPosY;
	std::vector<Texture*> mDialogTextures;

	Map* mCurrentMap;

	//Camera is the middle of the screen (unit = blocks)
	float mCameraPosX;
	float mCameraPosY;

	//Player
	Creature_Player* mPlayer;
	bool* mButtonPressed;
	static const float SPAWN_X;
	static const float SPAWN_Y;

	//Entities
	std::vector<Dynamic*> mEntities;
	std::vector<Dynamic*> mChanging;

	//Script processor
	ScriptProcessor mScript;

	//Quests
	std::list<Quest*> mQuests;

    //Display
    Font* mFont;

    //Modes
    enum
    {
    	MODE_TITLE,
    	MODE_LOCAL_MAP,
    	MODE_WORLD_MAP,
    	MODE_INVENTORY,
    	MODE_SHOP
    };
    int mGameMode;

    // [[[ INVENTORY MODE ]]]
    //Inventory
    int mSelectedItemX;
    int mSelectedItemY;

	//TESTS

};

#endif // GAME_H_INCLUDED
