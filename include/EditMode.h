#ifndef EDITMODE_H_INCLUDED
#define EDITMODE_H_INCLUDED

#include <SDL2/SDL.h>
#include <iostream>
#include <string>
#include <sstream>

#include "rpg/Map.h"
#include "usefullPack/Assets.h"
#include "usefullPack/TextureManager.h"
#include "usefullPack/Menu.h"

/*GLOBAL CONSTANTS*/
const int TEXTURE_TILE_SIZE = 16; //The number of pixels for a tile on the texture sheet

class EditMode
{
public:
	static const int SCREEN_WIDTH = 1000;
	static const int SCREEN_HEIGHT = 640;

	EditMode();
	~EditMode();

	bool initialize(SDL_Window* window, SDL_Renderer* renderer);

	void update(float elapsedTime);
	void save();

	bool isRunning();

	bool changeMap(Map* m);

	//Display coordinates
	void updateCoordinates(int x, int y);
	void displayCoordinates();

private:
	bool mRunning;

	//Map
	Map* mCurrentMap;
	bool mDisplaySolid;
	int mTileWidth, mTileHeight;

	//Informations to change a block
	SDL_Rect* mCurrentBlock;
	SDL_Point* mBlockToChange;
	bool mCurrentBlockIsSolid;

	//Display
	Font* mFont;

	//Menu
	Menu* mMenu;
	bool mDisplayMenu;

	//Display the coordinates
	Texture* mCoordinates;

	//Display the tile chosen
	bool mDisplayTileChosen;
	int mPosTileChosenX, mPosTileChosenY;

	//Camera is the middle of the screen
	float mCameraPosX, mCameraPosY;
};

#endif // EDITMODE_H_INCLUDED
