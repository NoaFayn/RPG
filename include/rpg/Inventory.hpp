#ifndef INVENTORY_HPP_INCLUDED
#define INVENTORY_HPP_INCLUDED

#include <vector>
#include <algorithm>

#include "objects/Item.hpp"

class Inventory
{
public:
	Inventory();

	bool giveItem(Item* item);
	bool takeItem(Item* item);
	bool hasItem(Item* item);

	std::vector<Item*> getAllItems();

private:
	std::vector<Item*> mItems;
};

#endif // INVENTORY_HPP_INCLUDED
