#ifndef QUEST_MAIN_H_INCLUDED
#define QUEST_MAIN_H_INCLUDED

#include "rpg/Quests/Quest_Skelly.h"
#include "objects/Creature.h"
#include "commands/Command_AddQuest.h"
#include "objects/Dynamics/Dynamic_Item.hpp"

class Quest_Main : public Quest
{
public:
	Quest_Main();

	bool populateDynamics(std::vector<Dynamic*> &dynamics, std::string mapName) override;
	bool onInteraction(std::vector<Dynamic*> &dynamics, Dynamic* target, Interactions nature) override;

private:
	int mPhase;
};

#endif // QUEST_MAIN_H_INCLUDED
