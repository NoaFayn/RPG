#ifndef QUEST_SKELLY_H_INCLUDED
#define QUEST_SKELLY_H_INCLUDED

#include "rpg/Quest.h"
#include "objects/Creatures/Creature_Skeleton.h"
#include "objects/Dynamics/Dynamic_Teleport.h"

class Quest_SkellyQuest : public Quest
{
public:
	Quest_SkellyQuest();

	bool populateDynamics(std::vector<Dynamic*> &dynamics, std::string mapName) override;
	bool onInteraction(std::vector<Dynamic*> &dynamics, Dynamic* target, Interactions nature) override;

private:
	int mPhase;
};

#endif // QUEST_SKELLY_H_INCLUDED
