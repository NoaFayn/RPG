#ifndef MAP_H_INCLUDED
#define MAP_H_INCLUDED

#include <SDL2/SDL.h>
#include <string>
#include <fstream>
#include <vector>
#include <iostream>

#include "commands/ScriptProcessor.h"
#include "objects/Dynamic.h"
#include "usefullPack/TextureManager.h"
#include "objects/Dynamics/Dynamic_Teleport.h"
#include "commands/Command.h"
#include "commands/Command_ChangeMap.h"
#include "objects/Creatures/Creature_Skeleton.h"
#include "objects/Dynamics/Dynamic_Item.hpp"
#include "objects/Dynamics/Dynamic_Sign.hpp"

class Assets;

class Map
{
public:
	enum Interactions
	{
		TALK,
		WALK
	};

public:
	Map();
	virtual ~Map();

	static void initilize(ScriptProcessor* script);

	/**Load a map
	* @param fileData A file (.txt) with all the informations to generate the map. See the convention file to know how to create a fileData.
	* @param texture This is the texture with all the tiles on it.
	* @param name The name of the map. (this is just for RPG)*/
	bool load(std::string fileData, Texture* texture, std::string name);
	bool create(std::string fileData, Texture* texture, int sizeMapX, int sizeMapY);
	bool save();
	int getIndex(int x, int y);
	bool getSolid(int x, int y);

	virtual bool populateDynamics(std::vector<Dynamic*> &dynamics);

	virtual bool onInteraction(std::vector<Dynamic*> &dynamics, Dynamic* target, Interactions nature);

	void setType(int x, int y, int type);
	void setSolid(int x, int y, bool isSolid);


	/**Get the width of the map (in blocks)*/
	int getNbBlocksWidth();
	/**Get the height of the map (in blocks)*/
	int getNbBlocksHeight();
	std::string getName();
	Texture* getTexture();

protected:
	static ScriptProcessor* gScript;

private:
	std::string mName; //Name of the map
	std::string mPath; //File location
	Texture* mTexture; //Texture
	int mWidth, mHeight; //Width and height of the map  (in blocks)
	bool* mSolids; //Array of booleans to check if a tile map is solid
	int* mIndices; //Array of integers storing the indexes of the tile map
};



class map_Village1 : public Map
{
public:
	map_Village1();
	~map_Village1();

	bool populateDynamics(std::vector<Dynamic*> &dynamics) override;
	bool onInteraction(std::vector<Dynamic*> &dynamics, Dynamic* target, Interactions nature) override;
};


class map_Labo1 : public Map
{
public:
	map_Labo1();
	~map_Labo1();

	bool populateDynamics(std::vector<Dynamic*> &dynamics) override;
	bool onInteraction(std::vector<Dynamic*> &dynamics, Dynamic* target, Interactions nature) override;
};

#endif // MAP_H_INCLUDED
