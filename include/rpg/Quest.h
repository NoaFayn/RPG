#ifndef QUEST_H_INCLUDED
#define QUEST_H_INCLUDED

#include <string>
#include <vector>

#include "commands/ScriptProcessor.h"
#include "objects/Dynamic.h"
#include "commands/Command_ShowDialog.h"
#include "commands/Command_MoveTo.h"

class Game;
class Command_AddQuest;

class Quest
{
public:
	enum Interactions
	{
		TALK,
		ATTACK,
		KILL,
		WALK
	};

public:
	Quest();

	static void initialize(ScriptProcessor* script, Game* gameEngine);

	virtual bool populateDynamics(std::vector<Dynamic*> &dynamics, std::string mapName);
	virtual bool onInteraction(std::vector<Dynamic*> &dynamics, Dynamic* target, Interactions nature);

	//Getters
	std::string getName() const;
	bool isCompleted() const;

protected:
	static ScriptProcessor* gScript;
	static Game* gGameEngine;

	std::string mName;

	bool mCompleted;
};

#endif // QUEST_H_INCLUDED
