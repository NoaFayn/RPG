#ifndef ASSETS_H_INCLUDED
#define ASSETS_H_INCLUDED

#include <string>
#include <map>
#include <iostream>

#include "usefullPack/TextureManager.h"
#include "usefullPack/Font.h"
#include "objects/Item.hpp"

class Map;
class Item_Health;
class Item_HealthBoost;
class Item_Key;


enum Textures
{
	TEX_DYN_PLAYER,
	TEX_DYN_GHOST,
	TEX_DYN_SKELETON,
	TEX_DYN_SIGN,

	TEX_MAP_VILLAGE1,
	TEX_MAP_GLOBAL,

	TEX_MEN_MAIN,

	TEX_LOOT_SWORD,

	TEX_BAR_HEALTH,

	TEX_ITEM_HEALTH,
	TEX_ITEM_HEALTH_BOOST,
	TEX_ITEM_KEY
};

enum FontsId
{
	FNT_RPG,
	FNT_GLOBAL
};


class Assets
{
public:
	static Assets& getInstance();

	//Ensure that the instance remain persistent
	Assets(Assets const&) = delete;
	void operator=(Assets const&) = delete;

	bool loadTextures();
	bool loadMaps();
	bool loadFonts();
	bool loadItems();

	Texture* getTexture(Textures textureId);
	Map* getMap(std::string mapName);
	Font* getFont(FontsId fontId);
	Item* getItem(std::string itemName);

private:
	Assets();
	~Assets();

	bool loadTexture(Textures id, std::string path);
	void loadMap(Map* m);
	bool loadFont(FontsId id, std::string path);
	void loadItem(Item* i);

private:
	std::map<Textures, Texture*> mTextures;
	std::map<std::string, Map*> mMaps;
	std::map<FontsId, Font*> mFonts;
	std::map<std::string, Item*> mItems;
};

#endif // ASSETS_H_INCLUDED
