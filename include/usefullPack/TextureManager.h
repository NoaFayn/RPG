#ifndef TEXTUREMANAGER_H_INCLUDED
#define TEXTUREMANAGER_H_INCLUDED

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <string>
#include <iostream>

#include "usefullPack/Font.h"

class Texture
{
public:
	Texture();
	~Texture();

	static bool initialize(SDL_Window* window, SDL_Renderer* renderer);

	bool loadFromFile(std::string path);

	bool loadFromRenderedText(std::string textureText, Font* font);

	void free();

	void setColor(Uint8 red, Uint8 green, Uint8 blue);
	void setBlendMode(SDL_BlendMode blending);
	void setAlpha(Uint8 alpha);

	void render(SDL_Rect* src, SDL_Rect* dest);

	int getWidth();
	int getHeight();

	void rotation(double angle);
	void changeCenter(SDL_Point* center);
	void flip(SDL_RendererFlip flip);

public:
	static SDL_Window* gWindow;
	static SDL_Renderer* gRenderer;
	static int gTileSizeWidth;
	static int gTileSizeHeight;

private:
	SDL_Texture* mTexture;

	int mWidth;
	int mHeight;

	double mAngle;
	SDL_Point* mCenter;
	SDL_RendererFlip mFlip;
};





// === METHODS TO DRAW ===
void draw_fillRect(SDL_Rect fillRect, SDL_Color color);
void draw_lineRect(SDL_Rect fillRect, SDL_Color color);
void draw_line(int x1, int y1, int x2, int y2, int w, SDL_Color color);
void draw_text(std::string text, int x, int y, Font &f);

#endif // TEXTUREMANAGER_H_INCLUDED
