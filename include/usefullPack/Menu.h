#ifndef MENU_H_INCLUDED
#define MENU_H_INCLUDED

#include <vector>
#include <string>
#include <SDL2/SDL.h>

#include "usefullPack/TextureManager.h"
#include "usefullPack/Font.h"

class Menu
{
public:
	Menu(Texture* background);
	~Menu();

	void addChoices(std::vector<std::string> choices, Font* font);
	void addChoice(std::string choice, Font* font);

	void setTitle(std::string title, Font* font);

	void render();

private:
	Texture* mTitle;
	std::vector<Texture*> mChoices;

	Texture* mBackground;
};

#endif // MENU_H_INCLUDED
