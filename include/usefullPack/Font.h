#ifndef FONT_H_INCLUDED
#define FONT_H_INCLUDED

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

class Font
{
public:
	Font(TTF_Font* font);
	~Font();

	void changeColor(SDL_Color color);

	TTF_Font* getFont() const;
	SDL_Color getColor() const;

public:
	static SDL_Color sColorBlack;
	static SDL_Color sColorWhite;
	static SDL_Color sColorRed;

private:
	TTF_Font* mFont;
	SDL_Color mColor;

};

#endif // FONT_H_INCLUDED
