#ifndef Creature_H_INCLUDED
#define Creature_H_INCLUDED

#include <string>

#include "usefullPack/Assets.h"
#include "objects/Dynamic.h"


class Creature : public Dynamic
{
public:
	enum Directions {SOUTH, NORTH, WEST, EAST};
	enum States {STANDING, WALKING, DEAD, FIGHTING};

public:
	Creature(std::string name, Texture &texture);

	void draw(float offsetX, float offsetY, int tileSizeX, int tileSizeY) override;
	void update(float elapsedTime, Dynamic* player = nullptr) override;

	virtual void behaviour(float elapedTime, Creature* player = nullptr);

	void defineCharacteristics(int health, int attackDmg);
	void changeMaxHealth(int maxHealth);

	void attack(Creature* ennemy);
	void takeDamage(int dmg);
	void heal(int restored);

	void repop(float x, float y);

	bool isAlive();
	Directions getFacingDirection();

	void showHealthBar();

	int getHealthMax() const;
	int getHelath() const;

protected:
	Texture mTexture;

	//Health bar
	Texture* mHealthBar;
	static const float sTIME_TO_SHOW;
	float mTimeBarShown;
	bool mShowBar;

	Directions mFacingDirection;
	States mState;

	float mTimer;
	int mGraphicCounter;

	float mStateTick;

	//RPG
	int mHealth, mHealthMax;
	int mAttackDmg;

	bool mFighting;
	float mAnimFightDelay;

protected:
	static const int FRAMES_BAR = 7;
};

#endif // Creature_H_INCLUDED
