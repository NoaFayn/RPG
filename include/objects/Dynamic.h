#ifndef DYNAMIC_H_INCLUDED
#define DYNAMIC_H_INCLUDED

#include <string>


class Dynamic
{
public:
	Dynamic(std::string name);
	~Dynamic();

	virtual void draw(float offsetX, float offsetY, int tileSizeX, int tileSizeY) {}
	virtual void update(float elapsedTime, Dynamic* player = nullptr) {}

	virtual void onInteraction(Dynamic* player = nullptr) {}

	//Getters
	bool isFriendly() const;
	std::string getName() const;

	float getPosX() const;
	float getPosY() const;

	float getVelX() const;
	float getVelY() const;

	bool isSolidVsMap() const;
	bool isSolidVsDyn() const;

	bool canBeDestroyed() const;

	void setCenter(float x, float y);
	void setPosition(float x, float y);

	void addVelX(float x);
	void addVelY(float y);

	void stopMoving();

protected:
	float mPosX, mPosY;
	float mVelX, mVelY;

	bool mSolidVsMap;
	bool mSolidVsDyn;

	bool mFriendly;

	std::string mName;

	//TODO: remove
	bool mCleanable;
};







#endif // DYNAMIC_H_INCLUDED
