#ifndef DYNAMIC_SIGN_HPP_INCLUDED
#define DYNAMIC_SIGN_HPP_INCLUDED

#include "objects/Dynamic.h"
#include "usefullPack/TextureManager.h"
#include "usefullPack/Assets.h"

class Dynamic_Sign : public Dynamic
{
public:
	Dynamic_Sign(std::string message);

	void draw(float offsetX, float offsetY, int tileSizeX, int tileSizeY);
	void update(float elapsedTime, Dynamic* player = nullptr);

	void onInteraction(Dynamic* player = nullptr);

private:
	static const float sSHOW_TIME;

	std::string mMessage;
	Texture mTexture;
	bool mDrawMessage;
	float mTimeLeftToShow;
};

#endif // DYNAMIC_SIGN_HPP_INCLUDED
