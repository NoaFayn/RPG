#ifndef DYNAMIC_TELEPORT_H_INCLUDED
#define DYNAMIC_TELEPORT_H_INCLUDED

#include <string>

#include "objects/Dynamic.h"
#include <SDL2/SDL.h>
#include "usefullPack/TextureManager.h"

class DynamicTeleport : public Dynamic
{
public:
	DynamicTeleport(float x, float y, std::string mapName, float destX, float destY);

	void draw(float offsetX, float offsetY, int tileSizeX, int tileSizeY) override;

	void update(float elapsedTime, Dynamic* player = nullptr) override;

	//Getters
	std::string getMapName();
	float getSpawnX();
	float getSpawnY();

private:
	std::string mMapName;

	float mDestPosX, mDestPosY;
};

#endif // DYNAMIC_TELEPORT_H_INCLUDED
