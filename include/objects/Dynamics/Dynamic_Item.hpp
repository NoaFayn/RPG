#ifndef DYNAMIC_ITEM_HPP_INCLUDED
#define DYNAMIC_ITEM_HPP_INCLUDED

#include "objects/Dynamic.h"
#include "objects/Item.hpp"
#include "objects/Creatures/Creature_Player.hpp"

class Dynamic_Item : public Dynamic
{
public:
	Dynamic_Item(float x, float y, Item* item);

	void draw(float offsetX, float offsetY, int tileSizeX, int tileSizeY) override;

	void onInteraction(Dynamic* player = nullptr) override;

private:
	Item* mItem;
	bool mCollected;
};

#endif // DYNAMIC_ITEM_HPP_INCLUDED
