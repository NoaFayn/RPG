#ifndef ITEM_HEALTH_HPP_INCLUDED
#define ITEM_HEALTH_HPP_INCLUDED

#include "objects/Item.hpp"
#include "objects/Creature.h"

class Item_Health : public Item
{
public:
	Item_Health();

	bool onInteract(Dynamic* object) override;
	bool onUse(Dynamic* object) override;
};

#endif // ITEM_HEALTH_HPP_INCLUDED
