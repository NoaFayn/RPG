#ifndef ITEM_HEALTHBOOST_HPP_INCLUDED
#define ITEM_HEALTHBOOST_HPP_INCLUDED

#include "objects/Item.hpp"
#include "objects/Creature.h"

class Item_HealthBoost : public Item
{
public:
	Item_HealthBoost();

	bool onInteract(Dynamic* object) override;
	bool onUse(Dynamic* object) override;
};

#endif // ITEM_HEALTHBOOST_HPP_INCLUDED
