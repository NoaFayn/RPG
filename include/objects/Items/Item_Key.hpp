#ifndef ITEM_KEY_HPP_INCLUDED
#define ITEM_KEY_HPP_INCLUDED

#include "objects/Item.hpp"
#include "usefullPack/Assets.h"

class Item_Key : public Item
{
public:
	Item_Key();

    bool onInteract(Dynamic* object);
    bool onUse(Dynamic* object);

private:

};

#endif // ITEM_KEY_HPP_INCLUDED
