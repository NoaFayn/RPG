#ifndef ITEM_HPP_INCLUDED
#define ITEM_HPP_INCLUDED

#include <string>

#include "objects/Dynamic.h"
#include "usefullPack/TextureManager.h"

class Item
{
public:
	Item(std::string name, Texture &texture, std::string description);

	/**@param object interacts with the item
	@return TRUE if the object go to the player's inventory
	@return FALSE if the object don't go to the player's inventory*/
    virtual bool onInteract(Dynamic* object) = 0;
    /**@param object consume the item
    @return TRUE if the item is consumed and has to disappear
    @return FALSE if the item is not deleted*/
    virtual bool onUse(Dynamic* object) = 0;

	std::string getName() const;
	std::string getDescription() const;
	Texture* getTexture();
	bool isKeyItem() const;
	bool isEquipable() const;

protected:
	std::string mName;
	std::string mDescription;
	Texture mTexture;
	bool mKeyItem; //if TRUE the item cannot be used
	bool mEquipable;
};

#endif // ITEM_HPP_INCLUDED
