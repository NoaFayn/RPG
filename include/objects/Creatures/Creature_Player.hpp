#ifndef CREATURE_PLAYER_HPP_INCLUDED
#define CREATURE_PLAYER_HPP_INCLUDED

#include "objects/Creature.h"
#include "rpg/Inventory.hpp"

class Creature_Player : public Creature
{
public:
	Creature_Player();

	Inventory* getInventory();

private:
	Inventory mInventory;
};

#endif // CREATURE_PLAYER_HPP_INCLUDED
