#ifndef CREATURE_SKELETON_H_INCLUDED
#define CREATURE_SKELETON_H_INCLUDED

#include "objects/Creature.h"
#include <math.h>

class Creature_Skeleton : public Creature
{
public:
	Creature_Skeleton();

	void behaviour(float elapedTime, Creature* player = nullptr) override;
};

#endif // CREATURE_SKELETON_H_INCLUDED
