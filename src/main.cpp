#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <string>
#include <iostream>

#include "Game.h"
#include "usefullPack/Timer.h"
#include "EditMode.h"
//#include "constants.h"

using namespace std;

/*GLOBAL CONSTANTS*/
const string TITLE = "Game";

/*GLOBAL VARIABLES*/
SDL_Window* gWindow;
SDL_Renderer* gRenderer;

/*METHODS*/
bool init();
void close();

void launchGame();
void launchEditMode();

int main()
{
	init();
	SDL_HideWindow(gWindow);

	cout << "Enter mode: ";
	string mode;
	getline(cin, mode);

	if(mode == "game")
		launchGame();
	else if(mode == "edit")
		launchEditMode();

	close();
	return 0;
}



void launchGame()
{
	Game game;
	if(game.initialize(gWindow, gRenderer))
	{
		SDL_SetWindowSize(gWindow, Game::SCREEN_WIDTH, Game::SCREEN_HEIGHT);
		SDL_SetWindowPosition(gWindow, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED);
		SDL_ShowWindow(gWindow);

		Timer timer;

		while(game.isRunning())
		{
			float ellapsedTime = timer.getTicks() / 1000.0f;
			timer.start();
			game.update(ellapsedTime);
		}
	}
}

void launchEditMode()
{
	bool loop = true;
	while(loop)
	{
		EditMode editMode;
		editMode.initialize(gWindow, gRenderer);

		Map* m = new Map();
		bool mapLoaded = false;

		//Get the map name
		string mapToLoad;
		cout << "Name map to load: ";
		getline(cin, mapToLoad);

		//Quit
		if(mapToLoad == "quit" || mapToLoad == "q")
			loop = false;

		//Map existing in Assets
		if(loop)
		{
			m = Assets::getInstance().getMap(mapToLoad);
			mapLoaded = (m != nullptr);
		}

		//Map created by the user
		if(!mapLoaded && loop)
		{
			m = new Map();
			mapLoaded = m->load("assets/maps/" + mapToLoad, Assets::getInstance().getTexture(Textures::TEX_MAP_GLOBAL),mapToLoad);
		}

		//Create the map
		if(!mapLoaded && loop)
		{
			cout << "Error: this map does not exists." << endl;
			cout << "Do you want to create this map?" << endl;
			string answer;
			getline(cin, answer);
			if(answer == "oui" || answer == "Oui" || answer == "OUI" || answer == "yes" || answer == "Yes" || answer == "YES" || answer == "o" || answer == "y")
			{
				cout << "Try to create the map..." << endl;
				cout << "Enter nb of tiles X: ";
				int nbTilesX = 0;
				cin >> nbTilesX;
				cout << "Enter nb of tiles Y: ";
				int nbTilesY = 0;
				cin >> nbTilesY;
				if(!m->create("assets/maps/" + mapToLoad, Assets::getInstance().getTexture(Textures::TEX_MAP_GLOBAL), nbTilesX, nbTilesY))
				{
					cout << "Failed to create map " << mapToLoad << "!" << endl;
					mapLoaded = false;
				}
				else
				{
					m->load("assets/maps/" + mapToLoad, Assets::getInstance().getTexture(Textures::TEX_MAP_GLOBAL), mapToLoad);
					cout << "Map created successfully!" << endl;
					mapLoaded = true;
				}
			}
		}

		if(mapLoaded && loop)
		{
			editMode.changeMap(m);

			SDL_SetWindowSize(gWindow, EditMode::SCREEN_WIDTH, EditMode::SCREEN_HEIGHT);
			SDL_SetWindowPosition(gWindow, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED);
			SDL_ShowWindow(gWindow);

			Timer timer;

			while(editMode.isRunning())
			{
				float elapsedTime = timer.getTicks() / 1000.0f;
				timer.start();
				editMode.update(elapsedTime);
			}
			mapLoaded = false;
			SDL_HideWindow(gWindow);
		}
	}
}



bool init()
{
	//Initialize SDL
	if(SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		cout << "SDL could not initialize!\nERROR: " << SDL_GetError() << endl;
		return false;
	}

	//Initialize SDL image
	if(IMG_Init(IMG_INIT_PNG) < 0)
	{
		cout << "SDL_IMAGE could not initialize!\nERROR: " << IMG_GetError() << endl;
		return false;
	}

	//Initialize SDL ttf
	if(TTF_Init() < 0)
	{
		cout << "SDL_TTF could not initialize!\nERROR: " << TTF_GetError() << endl;
		return false;
	}

	//Initialize window
	gWindow = SDL_CreateWindow(TITLE.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 0, 0, SDL_WINDOW_SHOWN);
	if(gWindow == NULL)
	{
		cout << "Failed to create window!\nERROR: " << SDL_GetError() << endl;
		return false;
	}

	//Initialize renderer
	gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED);
	if(gRenderer == NULL)
	{
		cout << "Failed to create renderer!\nERROR: " << SDL_GetError() << endl;
		return false;
	}

	return true;
}


void close()
{
	SDL_DestroyWindow(gWindow);
	SDL_DestroyRenderer(gRenderer);

	TTF_Quit();
	IMG_Quit();
	SDL_Quit();
}
