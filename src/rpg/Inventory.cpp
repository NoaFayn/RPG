#include "rpg/Inventory.hpp"

Inventory::Inventory()
{

}

bool Inventory::giveItem(Item* item)
{
	mItems.push_back(item);
	return true;
}

bool Inventory::takeItem(Item* item)
{
	if(item != nullptr)
	{
		mItems.erase(std::find(mItems.begin(), mItems.end(), item));
		return true;
	}
	return false;
}

bool Inventory::hasItem(Item* item)
{
	if(item != nullptr)
	{
		return find(mItems.begin(), mItems.end(), item) != mItems.end();
	}
	return false;
}

std::vector<Item*> Inventory::getAllItems()
{
	return mItems;
}
