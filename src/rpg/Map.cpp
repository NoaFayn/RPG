#include "rpg/Map.h"

#include "usefullPack/Assets.h"

using namespace std;

//Define static variables
ScriptProcessor* Map::gScript = nullptr;

Map::Map()
{
	mTexture = NULL;
	mIndices = NULL;
	mSolids = NULL;
	mWidth = 0;
	mHeight = 0;
}

Map::~Map()
{
	delete[] mSolids;
	delete[] mIndices;
}

void Map::initilize(ScriptProcessor* script)
{
	gScript = script;
}

int Map::getNbBlocksHeight()
{
	return mHeight;
}

int Map::getNbBlocksWidth()
{
	return mWidth;
}

string Map::getName()
{
	return mName;
}

Texture* Map::getTexture()
{
	return mTexture;
}

bool Map::load(string fileData, Texture* texture, string name)
{
	mName = name;
	mTexture = texture;
	mPath = fileData;
	ifstream data(fileData, ios::in | ios::binary);
	//Check if the file is open
	if(!data.is_open())
		return false;

	//First 2 numbers are the width and the height
	data >> mWidth >> mHeight;
	//Create 2D arrays
	mIndices = new int[mWidth * mHeight];
	mSolids = new bool[mWidth * mHeight];
	//Read the all file
	for(int i = 0; i < mWidth * mHeight; i++)
	{
		//First number is the type of block
		data >> mIndices[i];
		//Second number is the solid boolean
		data >> mSolids[i];
	}

	data.close();
	return true;
}

bool Map::create(std::string fileData, Texture* texture, int sizeMapX, int sizeMapY)
{
	ofstream data(fileData, ios::out | ios::trunc);
	//Check if the file is open
	if(!data.is_open())
	{
		std::cout << "Unable to open the file " << fileData << "!" << std::endl;
		return false;
	}

	//Save the size of the map
	data << sizeMapX << " " << sizeMapY << " ";

	for(int i = 0; i < sizeMapX * sizeMapY; i++)
			data << "-1 0 ";

	data.close();

	//Initialize
	mIndices = new int[sizeMapX * sizeMapY];
	mSolids = new bool[sizeMapX * sizeMapY];
	/*
	for(int i = 0; i < sizeMapX * sizeMapY; i++)
	{
		mIndices[i] = -1;
		mSolids[i] = false;
	}
	*/

	std::cout << "Map created!" << std::endl;
	return true;
}

bool Map::save()
{
	ofstream data(mPath, ios::out | ios::trunc);
	//Check if the file is open
	if(!data.is_open())
	{
		std::cout << "Unable to open the file " << mPath << "!" << std::endl;
		return false;
	}
	//First 2 numbers for the size
	data << mWidth << " " << mHeight << " ";
	//Read the arrays
	for(int i = 0; i < mWidth * mHeight; i++)
	{
		//First number for the type of block
		data << mIndices[i] << " ";
		//Second for solid state
		//Borders of the map MUST be solid
		if(i % mWidth == 0 || i % mWidth == mWidth-1 || i / mWidth == 0 || i / mWidth == mHeight-1)
			data << true << " ";
		else
			data << mSolids[i] << " ";
	}

	std::cout << "Saved!" << endl;
	data.close();
	return true;
}

int Map::getIndex(int x, int y)
{
	//Verify if the x and y are correct
	if(x >= 0 && x < mWidth && y >= 0 && y < mHeight)
	{
		return mIndices[y*mWidth+x];
	}
	return -1;
}

bool Map::getSolid(int x, int y)
{
	//Verify if the x and y are correct
	if(x >= 0 && x < mWidth && y >= 0 && y < mHeight)
	{
		return mSolids[y*mWidth+x];
	}
	//TODO: handle exceptions!
	return false;
}

bool Map::populateDynamics(std::vector<Dynamic* >& dynamics)
{
	return false;
}

bool Map::onInteraction(std::vector<Dynamic*> &dynamics, Dynamic* target, Interactions nature)
{
	return false;
}

void Map::setType(int x, int y, int type)
{
	if(x >= 0 && x < mWidth && y >= 0 && y < mHeight)
		mIndices[y*mWidth+x] = type;
}

void Map::setSolid(int x, int y, bool isSolid)
{
	if(x >= 0 && x < mWidth && y >= 0 && y < mHeight)
		mSolids[y*mWidth+x] = isSolid;
}







//=============
//| VILLAGE 1 |
//=============

map_Village1::map_Village1() : Map()
{
	load("./assets/map_village1.txt", Assets::getInstance().getTexture(Textures::TEX_MAP_GLOBAL), "Village 1");
}

map_Village1::~map_Village1() {}

bool map_Village1::populateDynamics(std::vector<Dynamic*> &dynamics)
{
	//Add items
	Dynamic* item = new Dynamic_Item(10.0f, 10.0f, Assets::getInstance().getItem("Health"));
	dynamics.push_back(item);
	Dynamic* itemHealthBoost = new Dynamic_Item(6.0f, 10.0f, Assets::getInstance().getItem("Health Boost"));
	dynamics.push_back(itemHealthBoost);

	//Add sign
	Dynamic* sign = new Dynamic_Sign("CAUTION: this house is haunted");
	sign->setPosition(13.0f, 7.5f);
	dynamics.push_back(sign);

	//Add enemy
	Dynamic* enemy = new Creature_Skeleton();
	enemy->setPosition(9, 16);
	dynamics.push_back(enemy);


	return true;
}

bool map_Village1::onInteraction(std::vector<Dynamic* >& dynamics, Dynamic* target, Interactions nature)
{
	if(target->getName() == "Teleport")
	{
		gScript->addCommand(new Command_ChangeMap(
			((DynamicTeleport*)target)->getMapName(),
			((DynamicTeleport*)target)->getSpawnX(),
			((DynamicTeleport*)target)->getSpawnY()));
		return true;
	}

	return false;
}













//==========
//| LABO 1 |
//==========
map_Labo1::map_Labo1() : Map()
{
	load("./assets/map_labo1.txt", Assets::getInstance().getTexture(Textures::TEX_MAP_GLOBAL), "Labo 1");
}

map_Labo1::~map_Labo1() {}

bool map_Labo1::populateDynamics(std::vector<Dynamic* >& dynamics)
{
	//Add teleporters
	dynamics.push_back(new DynamicTeleport(9.0f, 13.0f, "Village 1", 14.0f, 8.0f));
	dynamics.push_back(new DynamicTeleport(10.0f, 13.0f, "Village 1", 14.0f, 8.0f));

	return true;
}

bool map_Labo1::onInteraction(std::vector<Dynamic* >& dynamics, Dynamic* target, Interactions nature)
{
	if(target->getName() == "Teleport")
	{
		gScript->addCommand(new Command_ChangeMap(
			((DynamicTeleport*)target)->getMapName(),
			((DynamicTeleport*)target)->getSpawnX(),
			((DynamicTeleport*)target)->getSpawnY()));
		return true;
	}

	return false;
}

