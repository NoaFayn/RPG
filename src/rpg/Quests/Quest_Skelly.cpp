#include "rpg/Quests/Quest_Skelly.h"

// ==========
// | SKELLY |
// ==========
Quest_SkellyQuest::Quest_SkellyQuest()
{
	mPhase = 0;
}

bool Quest_SkellyQuest::populateDynamics(std::vector<Dynamic* >& dynamics, std::string mapName)
{
	if(mapName == "Village 1")
	{
		//Add teleporters
		dynamics.push_back(new DynamicTeleport(14.0f, 6.5f, "Labo 1", 10.0f, 12.0f));
	}

	if(mapName == "Labo 1")
	{
		//Add skelly
		Creature* npc = new Creature("Skelly", *Assets::getInstance().getTexture(Textures::TEX_DYN_SKELETON));
		npc->setPosition(9.0f, 11.0f);
		dynamics.push_back(npc);

		//Add enemies
		for(int i = 0; i < 3; i++)
		{
			Dynamic* enemie = new Creature_Skeleton();
			dynamics.push_back(enemie);
			enemie->setPosition(rand() % 8 + 5.0f,
							rand() % 3 + 6.0f);
		}
	}

	return true;
}

bool Quest_SkellyQuest::onInteraction(std::vector<Dynamic* >& dynamics, Dynamic* target, Interactions nature)
{

    if(target->getName() == "Casper")
	{
		gScript->addCommand(new Command_ShowDialog({"[Casper]", "You are doing Skelly's", "quest."}));
		return true;
	}


	return false;
}
