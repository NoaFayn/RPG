#include "rpg/Quests/Quest_Main.h"

// ========
// | MAIN |
// ========
Quest_Main::Quest_Main()
{
	mPhase = 0;
}

bool Quest_Main::populateDynamics(std::vector<Dynamic* >& dynamics, std::string mapName)
{
	if(mapName == "Village 1")
	{
		//Appear a npc
		Creature* npc = new Creature("Casper", *Assets::getInstance().getTexture(Textures::TEX_DYN_GHOST));
		npc->setPosition(6.0f, 4.0f);
		dynamics.push_back(npc);
	}
	else if(mapName == "Labo 1")
	{

	}

	return true;
}

bool Quest_Main::onInteraction(std::vector<Dynamic* >& dynamics, Dynamic* target, Interactions nature)
{
	if(mPhase == 0)
	{
		if(target->getName() == "Casper")
		{
			gScript->addCommand(new Command_ShowDialog({"[Casper]", "Look! This is Skelly!", "I think he has a quest", "for you!"}));
			//Create skelly
			Creature* npc = new Creature("Skelly", *Assets::getInstance().getTexture(Textures::TEX_DYN_SKELETON));
			npc->setPosition(6.0f, 1.0f);
			dynamics.push_back(npc);
			//Bring skelly
			gScript->addCommand(new Command_MoveTo(npc, 11.0, 1.0f, 2.0f));
			gScript->addCommand(new Command_MoveTo(npc, 11.0, 6.0f, 1.5f));
			gScript->addCommand(new Command_MoveTo(npc, 4.9, 6.0f, 2.0f));
			gScript->addCommand(new Command_MoveTo(npc, 5.0, 5.0f, 0.5f));

			mPhase = 1;

			return true;
		}
	}
	else if(mPhase == 1)
	{
		if(target->getName() == "Casper")
		{
			gScript->addCommand(new Command_ShowDialog({"[Casper]", "You should takl to him!"}));

			return true;
		}
		if(target->getName() == "Skelly")
		{
			gScript->addCommand(new Command_ShowDialog({"[Skelly]", "Hey! I've found a secret passage", "in this house!", "Would you like to", "come with me?"}));
			gScript->addCommand(new Command_ShowDialog({"[Skelly]", "But there are some", "skeletons guarding it.", "I hope you aren't affraid?"}));
			gScript->addCommand(new Command_ShowDialog({"[Skelly]", "So if you could just kill them", "so we can pass."}));
			gScript->addCommand(new Command_ShowDialog({"[Skelly]", "Here is the key to get inside!"}));

			//Appear the key
			Dynamic* itemKey = new Dynamic_Item(4.0f, 5.0f, Assets::getInstance().getItem("Key"));
			dynamics.push_back(itemKey);

			//Add the teleporter
			dynamics.push_back(new DynamicTeleport(14.0f, 6.5f, "Labo 1", 10.0f, 12.0f));

			gScript->addCommand(new Command_AddQuest(new Quest_SkellyQuest()));

			mPhase = 2;

			return true;
		}
	}



	return false;
}
