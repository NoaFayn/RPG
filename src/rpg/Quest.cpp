#include "rpg/Quest.h"

#include "Game.h"
#include "commands/Command_AddQuest.h"

//Define static variables
ScriptProcessor* Quest::gScript = nullptr;
Game* Quest::gGameEngine = nullptr;

Quest::Quest()
{
	mCompleted = false;
	mName = "UNDEFINED";

	if(gScript == NULL)
		std::cout << "WARNING: Creating a quest with SCRIPT NULL!!\nNeed to initialize first!" << std::endl;
	if(gGameEngine == NULL)
		std::cout << "WARNING: Creating a quest with GAME ENGINE NULL!!\nNeed to initialize first!" << std::endl;
}

void Quest::initialize(ScriptProcessor* script, Game* gameEngine)
{
	gScript = script;
	gGameEngine = gameEngine;
}

bool Quest::onInteraction(std::vector<Dynamic* >& dynamics, Dynamic* target, Interactions nature)
{
	return false;
}

bool Quest::populateDynamics(std::vector<Dynamic* >& dynamics, std::string mapName)
{

	return true;
}

std::string Quest::getName() const
{
	return mName;
}

bool Quest::isCompleted() const
{
	return mCompleted;
}
