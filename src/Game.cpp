#include "Game.h"

#include "rpg/Quest.h"
#include "commands/Command.h"

using namespace std;

//Initialize static constants
const float Game::SPAWN_X = 5.0f;
const float Game::SPAWN_Y = 5.0f;

Game::Game()
{}

bool Game::initialize(SDL_Window* window, SDL_Renderer* renderer)
{
	mRenderer = renderer;

	//Initialize textures
	if(!Texture::initialize(window, renderer))
	{
		cout << "[GAME_MODE] Unable to initialize TEXTURES!" << endl;
		mRunning = false;
		return false;
	}

	//Initialize assets
	if(!Assets::getInstance().loadTextures())
	{
		std::cout << "[GAME_MODE] Unable to initialize ASSETS <TEXTURES>!" << std::endl;
		mRunning = false;
		return false;
	}
	if(!Assets::getInstance().loadMaps())
	{
		std::cout << "[GAME_MODE] Unable to initialize ASSETS <MAPS>!" << std::endl;
		mRunning = false;
		return false;
	}
	if(!Assets::getInstance().loadFonts())
	{
		std::cout << "[GAME_MODE] Unable to initialize ASSETS <FONTS>!" << std::endl;
		mRunning = false;
		return false;
	}
	if(!Assets::getInstance().loadItems())
	{
		std::cout << "[GAME_MODE] Unable to initialize ASSETS <ITEMS>!" << std::endl;
		mRunning = false;
		return false;
	}

	//Initialize font
	mFont = Assets::getInstance().getFont(FontsId::FNT_GLOBAL);
	mFont->changeColor(Font::sColorWhite);

	//Initialize components
	Command::initialize(this);
	Map::initilize(&mScript);
	Quest::initialize(&mScript, this);

	//Initialize quest
	mQuests.push_front(new Quest_Main());

	//Initialize player
	mPlayer = new Creature_Player();
	mPlayer->defineCharacteristics(6, 2);
	mButtonPressed = new bool[4];
	for(int i = 0; i < 4; i++)
		mButtonPressed[i] = false;

	//Initialize map
	changeMap("Village 1", SPAWN_X, SPAWN_Y);

	//Initialize camera
	mCameraPosX = 0.0f;
	mCameraPosY = 0.0f;

	//Initialize dialog
	mShowDialog = false;
	mDialogPosX = mDialogPosY = 0.0f;

	//Game mode to default
	mGameMode = MODE_LOCAL_MAP;

	// [[[ MODE INVENTORY ]]]
	//Inventory
	mSelectedItemX = 0;
	mSelectedItemY = 0;

	mRunning = true;
	return true;
}

Game::~Game()
{
}

void Game::update(float elapsedTime)
{
	// [[[ RUN IN RIGHT MODE ]]]
	switch(mGameMode)
	{
	case MODE_TITLE:
		updateTitleScreen(elapsedTime);
		break;
	case MODE_LOCAL_MAP:
		updateLocalMap(elapsedTime);
		break;
	case MODE_WORLD_MAP:
		updateWorldMap(elapsedTime);
		break;
	case MODE_INVENTORY:
		updateInventory(elapsedTime);
		break;
	case MODE_SHOP:
		updateShop(elapsedTime);
		break;
	}
}

void Game::updateLocalMap(float elapsedTime)
{

	// [[[ HANDLE SCRIPTS ]]]
	mScript.processCommands(elapsedTime);



	// [[[ HANDLE EVENT ]]]
	const SDL_Keycode ACTION_BUTTON = SDLK_SPACE;

	SDL_Event event;
	while(SDL_PollEvent(&event) != 0)
	{
		switch(event.type)
		{
		case SDL_QUIT:
			mRunning = false;
			break;


		case SDL_KEYDOWN:
			if(event.key.keysym.sym == SDLK_ESCAPE)
				mRunning = false;

			if(mScript.hasUserControl() && !mPlayer->isAlive() && event.key.keysym.sym == ACTION_BUTTON)
			{
				//Reborn
				restart();
			}

			if(mScript.hasUserControl() && mPlayer->isAlive())
			{
				//User is in control
				if(event.key.keysym.sym == SDLK_UP)
				{
					mButtonPressed[0] = true;
					mButtonPressed[1] = false;
				}
				if(event.key.keysym.sym == SDLK_DOWN)
				{
					mButtonPressed[1] = true;
					mButtonPressed[0] = false;
				}
				if(event.key.keysym.sym == SDLK_LEFT)
				{
					mButtonPressed[2] = true;
					mButtonPressed[3] = false;
				}
				if(event.key.keysym.sym == SDLK_RIGHT)
				{
					mButtonPressed[3] = true;
					mButtonPressed[2] = false;
				}

				//Open inventory
				if(event.key.keysym.sym == SDLK_i)
					mGameMode = MODE_INVENTORY;


				if(event.key.keysym.sym == ACTION_BUTTON)
				{
					float interactorX = -1.0f, interactorY = -1.0f;
					switch(mPlayer->getFacingDirection())
					{
					case Creature::Directions::SOUTH:
						interactorX = mPlayer->getPosX() + 0.5f;
						interactorY = mPlayer->getPosY() + 1.5f;
						break;

					case Creature::Directions::WEST:
						interactorX = mPlayer->getPosX() - 0.5f;
						interactorY = mPlayer->getPosY() + 0.5f;
						break;

					case Creature::Directions::NORTH:
						interactorX = mPlayer->getPosX() + 0.5f;
						interactorY = mPlayer->getPosY() - 0.5f;
						break;

					case Creature::Directions::EAST:
						interactorX = mPlayer->getPosX() + 1.5f;
						interactorY = mPlayer->getPosY() + 0.5f;
						break;
					}

					//Check if interactor is a dynamic object
					bool interactionMade = false; //knowing if we have to spread the interaction or not
					for(auto dyn : mEntities)
					{
						if(interactorX > dyn->getPosX() && interactorX < dyn->getPosX() + 1.0f &&
						        interactorY > dyn->getPosY() && interactorY < dyn->getPosY() + 1.0f)
						{
							//Player can only interact with friendly objects
							if(dyn->isFriendly())
							{
								//If the interaction is quest related
								for(auto &quest : mQuests)
								{
									interactionMade = quest->onInteraction(mEntities, dyn, Quest::Interactions::TALK);
									if(interactionMade)
										break;
								}

								if(!interactionMade)
								{
									//Check if the interaction is map related
									interactionMade = mCurrentMap->onInteraction(mEntities, dyn, Map::Interactions::TALK);
								}
							}
							else
							{
								//TODO: ATTACK
								if(Creature* ennemy = dynamic_cast<Creature*>(dyn))
								{
									mPlayer->attack(ennemy);
								}
							}
						}
					}
				}
			}
			else
			{
				//Script system is in control
				if(mShowDialog)
				{
					if(event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_SPACE)
					{
						mShowDialog = false;
						mScript.terminateCommand();
					}
				}
			}
			break;


		case SDL_KEYUP:
			if(event.key.keysym.sym == SDLK_UP)
				mButtonPressed[0] = false;
			if(event.key.keysym.sym == SDLK_DOWN)
				mButtonPressed[1] = false;
			if(event.key.keysym.sym == SDLK_LEFT)
				mButtonPressed[2] = false;
			if(event.key.keysym.sym == SDLK_RIGHT)
				mButtonPressed[3] = false;
			break;
		}
	}


	//Change velocity
	if(mButtonPressed[0])
	{
		mPlayer->addVelY(-6.0f);
	}
	else if(mButtonPressed[1])
	{
		mPlayer->addVelY(6.0f);
	}
	else
	{
		mPlayer->addVelY(0.0f);
	}
	if(mButtonPressed[2])
	{
		mPlayer->addVelX(-6.0f);
	}
	else if(mButtonPressed[3])
	{
		mPlayer->addVelX(6.0f);
	}
	else
	{
		mPlayer->addVelX(0.0f);
	}





	//Calculations based on each entity
	for(auto &object : mEntities)
	{
		//Predict next position
		float nextPosX = object->getPosX() + object->getVelX() * elapsedTime;
		float nextPosY = object->getPosY() + object->getVelY() * elapsedTime;


		//Collision with map tiles
		if(object->isSolidVsMap()) //only if solid
		{
			const float margin = 0.01f; //margin for the object
			if(object->getVelX() <= 0) //toward the left
			{
				if(mCurrentMap->getSolid(nextPosX + margin, object->getPosY() + margin) || mCurrentMap->getSolid(nextPosX + margin, object->getPosY() + (1.0f - margin)))
				{
					nextPosX = (int)nextPosX + 1;
					object->addVelX(0.0f);
				}
			}
			else //toward right
			{
				if(mCurrentMap->getSolid(nextPosX + (1.0f - margin), object->getPosY() + margin) || mCurrentMap->getSolid(nextPosX + (1.0f - margin), object->getPosY() + (1.0f - margin)))
				{
					nextPosX = (int)nextPosX;
					object->addVelX(0.0f);
				}
			}
			if(object->getVelY() <= 0) //toward up
			{
				if(mCurrentMap->getSolid(nextPosX + margin, nextPosY + margin) || mCurrentMap->getSolid(nextPosX + (1.0f - margin), nextPosY + margin))
				{
					nextPosY = (int)nextPosY + 1;
					object->addVelY(0.0f);
				}
			}
			else //toward down
			{
				if(mCurrentMap->getSolid(nextPosX + margin, nextPosY + (1.0f - margin)) || mCurrentMap->getSolid(nextPosX + (1.0f - margin), nextPosY + (1.0f - margin)))
				{
					nextPosY = (int)nextPosY;
					object->addVelY(0.0f);
				}
			}
		}

		//Collision between dynamics objects
		float dynObjPosX = nextPosX;
		float dynObjPosY = nextPosY;

		for(auto &dyn : mEntities)
		{
			//No with itself
			if(dyn != object)
			{
				//If both objects are solid then collide!
				if(dyn->isSolidVsDyn() && object->isSolidVsDyn())
				{
					//Check if boundaries rectangles overlap
					if(dynObjPosX < dyn->getPosX() + 1.0f && dynObjPosX + 1.0f > dyn->getPosX() &&
					        object->getPosY() < dyn->getPosY() + 1.0f && object->getPosY() + 1.0f > dyn->getPosY())
					{
						//First check horizontally - Check left
						if(object->getVelX() <= 0)
							dynObjPosX = dyn->getPosX() + 1.0f;
						else
							dynObjPosX = dyn->getPosX() - 1.0f;
					}

					if(dynObjPosX < dyn->getPosX() + 1.0f && dynObjPosX + 1.0f > dyn->getPosX() &&
					        dynObjPosY < dyn->getPosY() + 1.0f && dynObjPosY + 1.0f > dyn->getPosY())
					{
						//First check vertically - Check up
						if(object->getVelY() <= 0)
							dynObjPosY = dyn->getPosY() + 1.0f;
						else
							dynObjPosY = dyn->getPosY() - 1.0f;
					}
				}
				else
				{
					//Interactions only with the player
					if(object == mPlayer)
					{
						//Check collision
						if(dynObjPosX < dyn->getPosX() + 1.0f && dynObjPosX + 1.0f > dyn->getPosX() &&
						        dynObjPosY < dyn->getPosY() + 1.0f && dynObjPosY + 1.0f > dyn->getPosY())
						{
							bool interactionMade = false;
							//If the interaction is quest related
							for(auto &quest : mQuests)
							{
								interactionMade = quest->onInteraction(mEntities, dyn, Quest::Interactions::WALK);
								if(interactionMade)
									break;
							}

							//Check if it is map related
							interactionMade = mCurrentMap->onInteraction(mEntities, dyn, Map::WALK);

							//Finally just check the object itself
							dyn->onInteraction(object);
						}
					}
				}
			}
		}



		//Update the object position after collision
		object->setPosition(dynObjPosX, dynObjPosY);
	}

	//Update the object
	for(auto &object : mEntities)
		object->update(elapsedTime, mPlayer);


	// [[[ DRAWING ]]]
	int visibleTilesX = Game::SCREEN_WIDTH / SIZE_TILE +1;
	int visibleTilesY = Game::SCREEN_HEIGHT / SIZE_TILE +1;


	//The camera follows the player
	mCameraPosX = mPlayer->getPosX();
	mCameraPosY = mPlayer->getPosY();

	// Calculate top-left most visible tile (unit = blocks)
	float offsetX = mCameraPosX - (float)visibleTilesX / 2.0f;
	float offsetY = mCameraPosY - (float)visibleTilesY / 2.0f;

	// Keep the camera to game boundaries (unit = blocks)
	if(offsetX > mCurrentMap->getNbBlocksWidth() - visibleTilesX) offsetX = mCurrentMap->getNbBlocksWidth() - visibleTilesX;
	if(offsetY > mCurrentMap->getNbBlocksHeight() - visibleTilesY) offsetY = mCurrentMap->getNbBlocksHeight() - visibleTilesY;
	if(offsetX < 0) offsetX = 0;
	if(offsetY < 0) offsetY = 0;

	//Calculate offsets for smooth movements
	float tileOffsetX = (offsetX - (int)offsetX) * SIZE_TILE;
	float tileOffsetY = (offsetY - (int)offsetY) * SIZE_TILE;


	// [[[ RENDER ]]]
	SDL_SetRenderDrawColor(mRenderer, 0x00, 0x00, 0x00, 0xFF);
	SDL_RenderClear(mRenderer);
	//vvv Render stuff here vvv

	// Draw visible tile map (unit = blocks)
	const int NB_CASES_TEXTURE_MAP_X = mCurrentMap->getTexture()->getWidth() / TILE_SIZE;
	for(int x = -1; x < visibleTilesX + 1; x++)
	{
		for(int y = -1; y < visibleTilesY + 1; y++)
		{
			int tileId = mCurrentMap->getIndex(x + offsetX, y + offsetY);

			//Only draw if the tileId is correct
			if(tileId != -1)
			{
				SDL_Rect src, dest;
				src.x = tileId % NB_CASES_TEXTURE_MAP_X * TILE_SIZE;
				src.y = tileId / NB_CASES_TEXTURE_MAP_X * TILE_SIZE;
				src.w = TILE_SIZE;
				src.h = TILE_SIZE;
				dest.x = x * SIZE_TILE - tileOffsetX;
				dest.y = y * SIZE_TILE - tileOffsetY;
				dest.w = SIZE_TILE;
				dest.h = SIZE_TILE;
				mCurrentMap->getTexture()->render(&src, &dest);
			}
		}
	}


	//Draw every entity
	for(unsigned int i = 1; i < mEntities.size(); i++)
		mEntities[i]->draw(offsetX, offsetY, SIZE_TILE, SIZE_TILE);
	//Draw the player on top of every entity
	mEntities[0]->draw(offsetX, offsetY, SIZE_TILE, SIZE_TILE);

	//Draw the dialog
	if(mShowDialog)
		displayDialog(50, 50);


	//^^^                   ^^^
	SDL_RenderPresent(mRenderer);




	// [[[ FREE MEMORY ]]]
	//Remove quests that have been completed
	auto i = remove_if(mQuests.begin(), mQuests.end(), [](const Quest* d)
	{
		return d->isCompleted();
	});
	if(i != mQuests.end()) //Except the main one
		mQuests.erase(i);

	//Remove items that have been used
	//TODO: fix this, not working
	/*
	auto i2 = remove_if(mEntities.begin(), mEntities.end(), [](const Dynamic* d)
	{
		return d->canBeDestroyed();
	});
	mEntities.erase(i2);
	*/
}








void Game::updateInventory(float elapsedTime)
{
	const int NB_ITEMS_DRAWN = 4;
	const int SIZE_ITEM = 64;
	const int SIZE_LINE = 20;
	const int OFFSET_DESCRIPTION = 400;
	Item* itemSelected = nullptr;


	//[[[ RENDERING ]]]
	//Clear screen
	SDL_SetRenderDrawColor(mRenderer, 0x00, 0x00, 0x00, 0xFF);
	SDL_RenderClear(mRenderer);

	//Draw text
	SDL_Color colorWhite = {0xFF, 0xFF, 0xFF, 0xFF};
	Assets::getInstance().getFont(FontsId::FNT_RPG)->changeColor(colorWhite);
	Assets::getInstance().getFont(FontsId::FNT_GLOBAL)->changeColor(colorWhite);

	draw_text("INVENTORY", 100, 20, *Assets::getInstance().getFont(FontsId::FNT_RPG));
	draw_text("SELECTED: ", 20, OFFSET_DESCRIPTION, *Assets::getInstance().getFont(FontsId::FNT_RPG));
	draw_text("DESCRIPTION: ", 20, OFFSET_DESCRIPTION + 2*SIZE_LINE, *Assets::getInstance().getFont(FontsId::FNT_RPG));


	//Draw items in the inventory
	int i = 0;
	for(auto &item : mPlayer->getInventory()->getAllItems())
	{
		int x = i % NB_ITEMS_DRAWN;
		int y = i / NB_ITEMS_DRAWN;
		i++;

		SDL_Rect dest;
		dest.x = 20 + x * SIZE_ITEM;
		dest.y = SIZE_LINE * 2 + y * SIZE_ITEM;
		dest.w = dest.h = SIZE_ITEM;
		item->getTexture()->render(NULL, &dest);

		if(mSelectedItemX == x && mSelectedItemY == y)
			itemSelected = item;
	}

	//Highlight selected item
	SDL_Rect lineRect;
	lineRect.x = 20 + mSelectedItemX * SIZE_ITEM;
	lineRect.y = SIZE_LINE * 2 + mSelectedItemY * SIZE_ITEM;
	lineRect.w = lineRect.h = SIZE_ITEM;
	draw_lineRect(lineRect, colorWhite);



	//Draw selected item's description
	if(itemSelected != nullptr)
	{
		draw_text(itemSelected->getName(), 25, OFFSET_DESCRIPTION + SIZE_LINE, *Assets::getInstance().getFont(FontsId::FNT_GLOBAL));

		draw_text(itemSelected->getDescription(), 25, OFFSET_DESCRIPTION + 3*SIZE_LINE, *Assets::getInstance().getFont(FontsId::FNT_GLOBAL));

		if(!itemSelected->isKeyItem())
		{
			draw_text("(Press SPACE to use)", 40, 500, *Assets::getInstance().getFont(FontsId::FNT_RPG));
		}
	}


	SDL_RenderPresent(mRenderer);










	// [[[ INPUTS ]]]
	SDL_Event event;
	while(SDL_PollEvent(&event) != 0)
	{
		switch(event.type)
		{
		case SDL_QUIT:
			mRunning = false;
			break;


		case SDL_KEYDOWN:
			switch(event.key.keysym.sym)
			{
			case SDLK_ESCAPE:
			case SDLK_i:
				mGameMode = MODE_LOCAL_MAP;
				break;
			case SDLK_RIGHT:
				mSelectedItemX = (mSelectedItemX + 1) % NB_ITEMS_DRAWN;
				break;
			case SDLK_LEFT:
				mSelectedItemX = (mSelectedItemX - 1 < 0 ? NB_ITEMS_DRAWN-1 : mSelectedItemX - 1);
				break;
			case SDLK_UP:
				mSelectedItemY = (mSelectedItemY - 1 < 0 ? NB_ITEMS_DRAWN-1 : mSelectedItemY - 1);
				break;
			case SDLK_DOWN:
				mSelectedItemY = (mSelectedItemY + 1) % NB_ITEMS_DRAWN;
				break;

			case SDLK_SPACE:
				//Use selected item
				if(itemSelected != nullptr)
				{
					if(!itemSelected->isKeyItem())
					{
						if(itemSelected->onUse(mPlayer))
						{
							mPlayer->getInventory()->takeItem(itemSelected);
						}
					}
				}
				break;
			}
		}
	}
}

void Game::updateShop(float elapsedTime)
{

}

void Game::updateTitleScreen(float elapsedTime)
{

}

void Game::updateWorldMap(float elapsedTime)
{

}





bool Game::isRunning()
{
	return mRunning;
}

void Game::showDialog(std::vector<std::string> lines)
{
	//Clear the previous dialog
	for(auto &i : mDialogTextures)
		delete i;
	mDialogTextures.clear();
	for(unsigned int i = 0; i < lines.size(); i++)
	{
		Texture* tex = new Texture();
		tex->loadFromRenderedText(lines[i], mFont);
		mDialogTextures.push_back(tex);
	}

	mDialogLinesToShow = lines;
	mShowDialog = true;
}

void Game::displayDialog(int x, int y)
{
	const int SIZE_LINE = 30;

	int maxLineLength = 0;
	int nbLines = mDialogTextures.size();

	//Find to longest line
	for(auto l : mDialogTextures) if(l->getWidth() > maxLineLength) maxLineLength = l->getWidth();

	//Draw a rectangle (background for the text)
	SDL_Rect fillRect = {x - 1, y - 1, maxLineLength + 1, nbLines * SIZE_LINE + 1};
	SDL_Color colorBlue = {0x00, 0x00, 0xFF, 0xFF};
	draw_fillRect(fillRect, colorBlue);
	//Draw the lines
	SDL_Color colorWhite = {0xFF, 0xFF, 0xFF, 0xFF};
	//TODO: changed on gitlab
	draw_lineRect(fillRect, colorWhite);
	/*
	draw_line(x - 1, y - 1, x + maxLineLength + 1, y - 1, 0, colorWhite);
	draw_line(x + maxLineLength + 1, y-1, x + maxLineLength + 1, y + nbLines * SIZE_LINE + 1, 0, colorWhite);
	draw_line(x + maxLineLength + 1, y + nbLines * SIZE_LINE + 1, x - 1, y + nbLines * SIZE_LINE + 1, 0, colorWhite);
	draw_line(x - 1, y + nbLines * SIZE_LINE + 1, x - 1, y - 1, 0, colorWhite);
    */

	//Draw the text
	for(int i = 0; i < nbLines; i++)
	{
		SDL_Rect dest = {x, y + SIZE_LINE * i, mDialogTextures[i]->getWidth(), SIZE_LINE};
		mDialogTextures[i]->render(NULL, &dest);
	}
}


void Game::changeMap(std::string mapName, float x, float y)
{
	//Destroy all dynamics
	mEntities.clear();

	//Replace the player
	mEntities.push_back(mPlayer);

	//Set current map
	mCurrentMap = Assets::getInstance().getMap(mapName);

	//Update player location
	mPlayer->setPosition(x, y);

	//Create new dynamics from map
	mCurrentMap->populateDynamics(mEntities);

	//Create new dynamics from quests
	for(auto q : mQuests)
		q->populateDynamics(mEntities, mCurrentMap->getName());
}

void Game::addQuest(Quest* quest)
{
	mQuests.push_front(quest);
}

void Game::summon(Dynamic* entity)
{
	mEntities.push_back(entity);
}

void Game::restart()
{
	changeMap("Village 1", SPAWN_X, SPAWN_Y);
	mPlayer->repop(SPAWN_X, SPAWN_Y);
}
