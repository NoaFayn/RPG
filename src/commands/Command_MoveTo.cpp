#include "commands/Command_MoveTo.h"

// ===================
// | MOVE TO COMMAND |
// ===================
Command_MoveTo::Command_MoveTo(Dynamic* object, float x, float y, float duration)
{
	mTargetPosX = x;
	mTargetPosY = y;
	mTimeSoFar = 0.0f;
	mDuration = std::max(duration, 0.001f);
	mObject = object;
}

void Command_MoveTo::start()
{
	mStarted = true;

	mStartPosX = mObject->getPosX();
	mStartPosY = mObject->getPosY();
}

void Command_MoveTo::update(float elapsedTime)
{
	mTimeSoFar += elapsedTime;
	float t = mTimeSoFar / mDuration;
	if(t > 1.0f) t = 1.0f;

	mObject->setPosition((mTargetPosX - mStartPosX) * t + mStartPosX,
						(mTargetPosY - mStartPosY) * t + mStartPosY);
	mObject->addVelX((mTargetPosX - mStartPosX) / mDuration);
	mObject->addVelY((mTargetPosY - mStartPosY) / mDuration);

	if(mTimeSoFar >= mDuration)
	{
		//Object has reached destination, stop
		//Place it at the right spot (to prevent from inaccuracies)
		mObject->setPosition(mTargetPosX, mTargetPosY);
		mObject->stopMoving();
		mTerminated = true;
	}
}
