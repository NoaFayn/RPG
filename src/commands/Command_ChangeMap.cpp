#include "commands/Command_ChangeMap.h"

#include "Game.h"

// ======================
// | CHANGE MAP COMMAND |
// ======================
Command_ChangeMap::Command_ChangeMap(std::string mapName, float spawnX, float spawnY)
{
	mMapName = mapName;
	mDestPosX = spawnX;
	mDestPosY = spawnY;
}

void Command_ChangeMap::start()
{
	mStarted = true;
	gGameEngine->changeMap(mMapName, mDestPosX, mDestPosY);
	mTerminated = true;
}
