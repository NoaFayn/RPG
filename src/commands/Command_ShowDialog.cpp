#include "commands/Command_ShowDialog.h"

#include "Game.h"

// =======================
// | SHOW DIALOG COMMAND |
// =======================
Command_ShowDialog::Command_ShowDialog(std::vector<std::string> lines)
{
	mLines = lines;
}

void Command_ShowDialog::start()
{
	mStarted = true;
	gGameEngine->showDialog(mLines);
}
