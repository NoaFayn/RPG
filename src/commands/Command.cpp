#include "commands/Command.h"

#include "Game.h"

//Initialize static variable
Game* Command::gGameEngine = nullptr;

Command::Command()
{
	mTerminated = false;
	mStarted = false;
}

Command::~Command()
{

}

void Command::initialize(Game* gameEngine)
{
	gGameEngine = gameEngine;
}

bool Command::isTerminated()
{
	return mTerminated;
}

bool Command::isStarted()
{
	return mStarted;
}

void Command::terminate()
{
	mTerminated = true;
}
