#include "commands/ScriptProcessor.h"


ScriptProcessor::ScriptProcessor()
{
	mUserControlEnable = true;
}

bool ScriptProcessor::hasUserControl()
{
	return mUserControlEnable;
}

void ScriptProcessor::addCommand(Command* cmd)
{
	mCommands.push_back(cmd);
}

void ScriptProcessor::processCommands(float elapsedTime)
{
	//If command are available, halt user control
	mUserControlEnable = mCommands.empty();

	if(!mCommands.empty())
	{
		//A command is available
		if(!mCommands.front()->isTerminated())
		{
			//Command has not been started
			if(!mCommands.front()->isStarted())
			{
				mCommands.front()->start();
			}
			else //Command has been started so process it
			{
				mCommands.front()->update(elapsedTime);
			}
		}
		else
		{
			//Command has been completed
			//TODO: verify on video if this is
			//delete mCommands.front();
			mCommands.pop_front();
		}
	}
}

void ScriptProcessor::terminateCommand()
{
    if(!mCommands.empty())
	{
		mCommands.front()->terminate();
	}
}
