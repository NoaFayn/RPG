#include "commands/Command_AddQuest.h"

#include "Game.h"
#include "rpg/Quest.h"

// =====================
// | ADD QUEST COMMAND |
// =====================
Command_AddQuest::Command_AddQuest(Quest* quest)
{
	mQuest = quest;
}

void Command_AddQuest::start()
{
	mStarted = true;
	gGameEngine->addQuest(mQuest);
	mTerminated = true;
}
