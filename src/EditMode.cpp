#include "EditMode.h"


EditMode::EditMode()
{
	mRunning = false;

	mCurrentMap = NULL;

	mCameraPosX = 0;
	mCameraPosY = 0;
}

EditMode::~EditMode()
{
	delete mCurrentMap;
	delete mCurrentBlock;
	delete mBlockToChange;
	delete mFont;
	delete mMenu;
	delete mCoordinates;
}

bool EditMode::initialize(SDL_Window* window, SDL_Renderer* renderer)
{
	//Initialize textures
	if(!Texture::initialize(window, renderer))
	{
		std::cout << "[EDIT_MODE] Unable to initialize TEXTURES!" << std::endl;
		mRunning = false;
		return false;
	}

	//Initialize assets
	if(!Assets::getInstance().loadTextures())
	{
		std::cout << "[EDIT_MODE] Unable to initialize ASSETS <TEXTURES>!" << std::endl;
		mRunning = false;
		return false;
	}
	if(!Assets::getInstance().loadMaps())
	{
		std::cout << "[EDIT_MODE] Unable to initialize ASSETS <MAPS>!" << std::endl;
		mRunning = false;
		return false;
	}
	if(!Assets::getInstance().loadFonts())
	{
		std::cout << "[EDIT_MODE] Unable to initialize ASSETS <FONTS>!" << std::endl;
		mRunning = false;
		return false;
	}

	//Initialize display
	mFont = Assets::getInstance().getFont(FontsId::FNT_RPG);
	mFont->changeColor(Font::sColorWhite);

	//Initialize menu
	mMenu = nullptr;
	mDisplayMenu = false;

	//Initialize map
	mCurrentMap = Assets::getInstance().getMap("Labo 1");
	mBlockToChange = nullptr;
	mDisplaySolid = false;
	mTileWidth = 30;
	mTileHeight = 30;

	//Next block to place
	mCurrentBlock = nullptr;
	mCurrentBlockIsSolid = false;

	//Coordinates
	mCoordinates = new Texture();
	mCoordinates->loadFromRenderedText("<0;0>", mFont);

	//Tile chosen
	mDisplayTileChosen = false;
	mPosTileChosenX = 0;
	mPosTileChosenY = 0;

	mRunning = true;
	return true;
}

bool EditMode::isRunning()
{
	return mRunning;
}


void EditMode::update(float elapsedTime)
{
	//Calculations
	//Number of tiles visible on the screen
	const int VISIBLE_TILES_MAP_X = SCREEN_WIDTH / 2 / mTileWidth;
	const int VISIBLE_TILES_MAP_Y = SCREEN_HEIGHT / mTileHeight;

	//Number of tiles on each row and column for the textures available
	const int NB_TILES_TEXTURE_X = mCurrentMap->getTexture()->getWidth() / TEXTURE_TILE_SIZE;
	const int NB_TILES_TEXTURE_Y = mCurrentMap->getTexture()->getHeight() / TEXTURE_TILE_SIZE;
	//Size in pixels of the tile of the texture available
	const int TILE_SIZE_TEXTURE_MAP = std::min(SCREEN_WIDTH/2/NB_TILES_TEXTURE_X, SCREEN_HEIGHT/NB_TILES_TEXTURE_Y);


	// [[ HANDLE INPUTS ]]
	SDL_Event event;
	while(SDL_PollEvent(&event) != 0)
	{
		switch(event.type)
		{
		case SDL_QUIT:
			mRunning = false;
			break;

		case SDL_KEYDOWN:
			switch(event.key.keysym.sym)
			{
			case SDLK_ESCAPE:
				//Quit the menu or the edit mode
				if(mDisplayMenu)
					mDisplayMenu = false;
				else
					mRunning = false;
				break;

			case SDLK_s:
				//Save the map modified
				save();
				break;

			case SDLK_a:
				//Display the tiles that are solid on the map
				mDisplaySolid = !mDisplaySolid;
				break;

			case SDLK_q:
				//Make the next block placed solid
				mCurrentBlockIsSolid = !mCurrentBlockIsSolid;
				break;

			case SDLK_p:
				//Increase the size of the view
				mTileWidth = std::min(mTileWidth + 5, 80);
				mTileHeight = std::min(mTileHeight + 5, 80);
				break;

			case SDLK_m:
				//Decrease the size of the view
				mTileWidth = std::max(mTileWidth - 5, 5);
				mTileHeight = std::max(mTileHeight - 5, 5);
				break;

			case SDLK_c:
				//Change the map
				{
					mMenu = new Menu(Assets::getInstance().getTexture(Textures::TEX_MEN_MAIN));
					mMenu->addChoices({"Village 1", "Labo 1"}, Assets::getInstance().getFont(FontsId::FNT_RPG));
					mMenu->setTitle("Choix de la map", Assets::getInstance().getFont(FontsId::FNT_RPG));
					mDisplayMenu = true;
				}
				break;

			//Camera motion
			case SDLK_UP:
				mCameraPosY -= 1.0f;
				break;
			case SDLK_DOWN:
				mCameraPosY += 1.0f;
				break;
			case SDLK_RIGHT:
				mCameraPosX += 1.0f;
				break;
			case SDLK_LEFT:
				mCameraPosX -= 1.0f;
				break;
			}
			break;

		case SDL_MOUSEMOTION:
			if(event.motion.x < SCREEN_WIDTH / 2)
			{
				//Update coordinates
				updateCoordinates(event.motion.x / mTileWidth + mCameraPosX, event.motion.y / mTileHeight + mCameraPosY);

				//Update tile chosen
				mPosTileChosenX = event.motion.x;
				mPosTileChosenY = event.motion.y;
				mDisplayTileChosen = true;
			}
		case SDL_MOUSEBUTTONDOWN:
			if(event.button.button == SDL_BUTTON_LEFT)
			{
				if(event.motion.x < SCREEN_WIDTH / 2 && mCurrentBlock != nullptr)
				{
					mBlockToChange = new SDL_Point();
					mBlockToChange->x = event.motion.x / mTileWidth + mCameraPosX;
					mBlockToChange->y = event.motion.y / mTileHeight + mCameraPosY;
				}
				else
				{
					mCurrentBlock = new SDL_Rect();
					mCurrentBlock->x = (event.motion.x - SCREEN_WIDTH / 2) / TILE_SIZE_TEXTURE_MAP;
					mCurrentBlock->y = event.motion.y / TILE_SIZE_TEXTURE_MAP;
					mCurrentBlock->w = mCurrentBlock->h = TILE_SIZE_TEXTURE_MAP;
				}
			}
		}
	}


	//Change the block
	if(mBlockToChange != NULL && mCurrentBlock != NULL)
	{
		mCurrentMap->setType(mBlockToChange->x, mBlockToChange->y, mCurrentBlock->y*NB_TILES_TEXTURE_X+mCurrentBlock->x);
		mCurrentMap->setSolid(mBlockToChange->x, mBlockToChange->y, mCurrentBlockIsSolid);
		mBlockToChange = nullptr;
	}



	//Keep the camera to game boundaries
	if(mCameraPosX > mCurrentMap->getNbBlocksWidth() - VISIBLE_TILES_MAP_X) mCameraPosX = mCurrentMap->getNbBlocksWidth() - VISIBLE_TILES_MAP_X;
	if(mCameraPosY > mCurrentMap->getNbBlocksHeight() - VISIBLE_TILES_MAP_Y) mCameraPosY = mCurrentMap->getNbBlocksHeight() - VISIBLE_TILES_MAP_Y;
	if(mCameraPosX < 0) mCameraPosX = 0;
	if(mCameraPosY < 0) mCameraPosY = 0;



	// [[[ RENDER ]]]
	SDL_SetRenderDrawColor(Texture::gRenderer, 0x00, 0x00, 0x00, 0xFF);
	SDL_RenderClear(Texture::gRenderer);
	//vvv Render stuff here vvv

	//Draw the menu on top of everything
	if(mDisplayMenu)
	{
		mMenu->render();
	}
	else
	{
		// Draw visible tile map (unit = blocks)
		const int NB_CASES_TEXTURE_MAP_X = mCurrentMap->getTexture()->getWidth() / TEXTURE_TILE_SIZE;
		for(int x = -1; x < VISIBLE_TILES_MAP_X + 1; x++)
		{
			for(int y = -1; y < VISIBLE_TILES_MAP_Y + 1; y++)
			{
				int tileId = mCurrentMap->getIndex(x + mCameraPosX, y + mCameraPosY);

				//Only draw if the tileId is correct
				if(tileId != -1)
				{
					SDL_Rect src, dest;
					src.x = tileId % NB_CASES_TEXTURE_MAP_X * TEXTURE_TILE_SIZE;
					src.y = tileId / NB_CASES_TEXTURE_MAP_X * TEXTURE_TILE_SIZE;
					src.w = TEXTURE_TILE_SIZE;
					src.h = TEXTURE_TILE_SIZE;
					dest.x = x * mTileWidth;
					dest.y = y * mTileHeight;
					dest.w = mTileWidth;
					dest.h = mTileHeight;
					mCurrentMap->getTexture()->render(&src, &dest);

					//Mark the tile if it is solid
					if(mDisplaySolid)
					{
						if(mCurrentMap->getSolid(x + mCameraPosX,y + mCameraPosY))
						{
							SDL_Rect fillRect;
							fillRect.w = fillRect.h = mTileWidth * 0.3f;
							fillRect.x = x * mTileWidth + mTileWidth / 2 - fillRect.w / 2;
							fillRect.y = y * mTileHeight;
							SDL_Color colorRed = {0xFF, 0x00, 0x00, 0xFF};
							draw_fillRect(fillRect, colorRed);
						}
					}
				}
			}
		}

		//Draw the tile chosen
		if(mDisplayTileChosen && mCurrentBlock != NULL)
		{
			SDL_Rect src, dest;
			src.x = mCurrentBlock->x * TEXTURE_TILE_SIZE;
			src.y = mCurrentBlock->y * TEXTURE_TILE_SIZE;
			src.w = TEXTURE_TILE_SIZE;
			src.h = TEXTURE_TILE_SIZE;
			dest.x = mPosTileChosenX - mTileWidth / 2;
			dest.y = mPosTileChosenY - mTileHeight / 2;
			dest.w = mTileWidth;
			dest.h = mTileHeight;
			mCurrentMap->getTexture()->render(&src, &dest);

			//Mark the tile if it is solid
			if(mCurrentBlockIsSolid)
			{
				SDL_Rect fillRect;
				fillRect.w = mTileWidth * 0.3f;
				fillRect.h = mTileHeight * 0.3f;
				fillRect.x = mPosTileChosenX - fillRect.w / 2;
				fillRect.y = dest.y;
				SDL_Color colorRed = {0xFF, 0x00, 0x00, 0xFF};
				draw_fillRect(fillRect, colorRed);
			}
		}


		//Draw textures tiles
		for(int i = 0; i < NB_TILES_TEXTURE_X; i++)
		{
			for(int j = 0; j < NB_TILES_TEXTURE_Y; j++)
			{
				SDL_Rect src, dest;
				src.x = i * TEXTURE_TILE_SIZE;
				src.y = j * TEXTURE_TILE_SIZE;
				src.w = TEXTURE_TILE_SIZE;
				src.h = TEXTURE_TILE_SIZE;
				dest.x = SCREEN_WIDTH / 2 + i * TILE_SIZE_TEXTURE_MAP;
				dest.y = j * TILE_SIZE_TEXTURE_MAP;
				dest.w = TILE_SIZE_TEXTURE_MAP;
				dest.h = TILE_SIZE_TEXTURE_MAP;
				mCurrentMap->getTexture()->render(&src, &dest);
			}
		}

		//Highlight current block
		if(mCurrentBlock != NULL)
		{
			SDL_Color colorRed = {0xFF, 0x00, 0x00, 0xFF};
			SDL_Rect lineRect;
			lineRect.x = mCurrentBlock->x * TILE_SIZE_TEXTURE_MAP + SCREEN_WIDTH / 2;
			lineRect.y = mCurrentBlock->y * TILE_SIZE_TEXTURE_MAP;
			lineRect.w = lineRect.h = TILE_SIZE_TEXTURE_MAP;
			draw_lineRect(lineRect, colorRed);

			//Mark if the tile is solid
			if(mCurrentBlockIsSolid)
			{
				SDL_Rect fillRect;
				fillRect.w = fillRect.h = TILE_SIZE_TEXTURE_MAP * 0.3f;
				fillRect.x = mCurrentBlock->x * TILE_SIZE_TEXTURE_MAP + TILE_SIZE_TEXTURE_MAP / 2 - fillRect.w / 2 + SCREEN_WIDTH / 2;
				fillRect.y = mCurrentBlock->y * TILE_SIZE_TEXTURE_MAP;
				SDL_Color colorRed = {0xFF, 0x00, 0x00, 0xFF};
				draw_fillRect(fillRect, colorRed);
			}
		}

		//Draw coordinates
		displayCoordinates();

	}


	//^^^                   ^^^
	SDL_RenderPresent(Texture::gRenderer);
}


void EditMode::save()
{
	if(!mCurrentMap->save())
	{
		std::cout << "[EditMode] Unable to save the map!" << std::endl;
	}
}

bool EditMode::changeMap(Map* m)
{
	if(m == nullptr)
	{
		std::cout << "[EditMode] Error: the map cannot be null" << std::endl;
		return false;
	}

	mCurrentMap = m;
	return true;
}

void EditMode::updateCoordinates(int x, int y)
{
	std::stringstream coord;
	coord << "<" << x << ";" << y << ">";
	mCoordinates->loadFromRenderedText(coord.str(), mFont);
}

void EditMode::displayCoordinates()
{
	const int margin = 5;
	SDL_Rect dest;
	dest.x = margin;
	dest.y = margin;
	dest.w = mCoordinates->getWidth();
	dest.h = mCoordinates->getHeight();
	mCoordinates->render(NULL, &dest);
}
