#include "objects/Dynamic.h"

Dynamic::Dynamic(std::string name)
{
	mName = name;
	mPosX = 0;
	mPosY = 0;
	mVelX = 0;
	mVelY = 0;

	mSolidVsDyn = true;
	mSolidVsMap = true;
	mFriendly = true;

	mCleanable = false;
}

Dynamic::~Dynamic() {}

std::string Dynamic::getName() const
{
	return mName;
}

bool Dynamic::isFriendly() const
{
	return mFriendly;
}

float Dynamic::getPosX() const
{
	return mPosX;
}

float Dynamic::getPosY() const
{
	return mPosY;
}

float Dynamic::getVelX() const
{
	return mVelX;
}

float Dynamic::getVelY() const
{
	return mVelY;
}

bool Dynamic::isSolidVsMap() const
{
	return mSolidVsMap;
}

bool Dynamic::isSolidVsDyn() const
{
	return mSolidVsDyn;
}

bool Dynamic::canBeDestroyed() const
{
	return mCleanable;
}

void Dynamic::setCenter(float x, float y)
{
    mPosX = x - 0.5f;
    mPosY = y - 0.5f;
}

void Dynamic::setPosition(float x, float y)
{
	mPosX = x;
	mPosY = y;
}

void Dynamic::addVelX(float x)
{
	mVelX = x;
}

void Dynamic::addVelY(float y)
{
	mVelY = y;
}

void Dynamic::stopMoving()
{
	mVelX = 0.0f;
	mVelY = 0.0f;
}
