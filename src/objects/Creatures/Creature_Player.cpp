#include "objects/Creatures/Creature_Player.hpp"

Creature_Player::Creature_Player() : Creature("Player", *Assets::getInstance().getTexture(Textures::TEX_DYN_PLAYER))
{
}

Inventory* Creature_Player::getInventory()
{
	return &mInventory;
}
