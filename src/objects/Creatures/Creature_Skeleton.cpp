#include "objects/Creatures/Creature_Skeleton.h"

// ============
// | SKELETON |
// ============
Creature_Skeleton::Creature_Skeleton() : Creature("Skelly", *Assets::getInstance().getTexture(Textures::TEX_DYN_SKELETON))
{
	mFriendly = false;
	mHealth = 10;
	mHealthMax = 10;

	mStateTick = 2.0f; //make a decision every 2sec
}

void Creature_Skeleton::behaviour(float elapedTime, Creature* player)
{
	mStateTick -= elapedTime;

	if(mStateTick <= 0.0f)
	{
		if(player->isAlive())
		{
			//Check if player is nearby
			float targetX = player->getPosX() - mPosX;
			float targetY = player->getPosY() - mPosY;
			float distance = sqrtf(targetX*targetX + targetY*targetY);

			const float RANGE_DETECTION = 6.0f;
			const float speed = 2.0f;
			if(distance < RANGE_DETECTION)
			{
				mVelX = (targetX/distance) * speed;
				mVelY = (targetY/distance) * speed;
			}
			else
			{
				mVelX = 0.0f;
				mVelY = 0.0f;
			}

			//If the player is close enough, attack him
			if((player->getPosX() + 1.0f > mPosX - 0.5f && player->getPosX() < mPosX + 1.5f && player->getPosY() + 1.0f > mPosY - 0.5f && player->getPosY() < mPosY + 1.0f) ||
			        (player->getPosX() + 1.0f > mPosX && player->getPosX() < mPosX + 1.0f && player->getPosY() + 1.0f > mPosY - 0.5f && player->getPosY() < mPosY + 1.5f))
			{
				attack(player);
			}
		}
		else
		{
            stopMoving();
		}

		//Reset the stateTick
		mStateTick += 1.0f;
	}
}
