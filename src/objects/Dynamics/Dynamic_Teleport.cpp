#include "objects/Dynamics/Dynamic_Teleport.h"

DynamicTeleport::DynamicTeleport(float x, float y, std::string mapName, float destX, float destY) : Dynamic("Teleport")
{
	mPosX = x;
	mPosY = y;

	mDestPosX = destX;
	mDestPosY = destY;

	mMapName = mapName;

	mSolidVsDyn = false;
	mSolidVsMap = false;
}

void DynamicTeleport::draw(float offsetX, float offsetY, int tileSizeX, int tileSizeY)
{
	//Nothing
	SDL_Rect lineRect;
	lineRect.x = (mPosX - offsetX) * tileSizeX;
	lineRect.y = (mPosY - offsetY) * tileSizeY;
	lineRect.w = tileSizeX;
	lineRect.h = tileSizeY;
	SDL_Color colorGreen = {0x00, 0xFF, 0x00, 0xFF};
    draw_lineRect(lineRect, colorGreen);
}

void DynamicTeleport::update(float elapsedTime, Dynamic* player)
{
	//Nothing
}

std::string DynamicTeleport::getMapName()
{
	return mMapName;
}

float DynamicTeleport::getSpawnX()
{
	return mDestPosX;
}

float DynamicTeleport::getSpawnY()
{
	return mDestPosY;
}
