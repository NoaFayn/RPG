#include "objects/Dynamics/Dynamic_Sign.hpp"

//Define static constants
const float Dynamic_Sign::sSHOW_TIME = 0.5f;

Dynamic_Sign::Dynamic_Sign(std::string message) : Dynamic("Sign")
{
	mMessage = message;

	mSolidVsDyn = false;
	mSolidVsMap = false;

	mTexture = *Assets::getInstance().getTexture(Textures::TEX_DYN_SIGN);
}

void Dynamic_Sign::draw(float offsetX, float offsetY, int tileSizeX, int tileSizeY)
{
	SDL_Rect dest;
	dest.x = (mPosX - offsetX) * tileSizeX;
	dest.y = (mPosY - offsetY) * tileSizeY;
	dest.w = tileSizeX;
	dest.h = tileSizeY;
	mTexture.render(NULL, &dest);

	if(mDrawMessage)
	{
		Texture* mes = new Texture();
		mes->loadFromRenderedText(mMessage, Assets::getInstance().getFont(FontsId::FNT_GLOBAL));
		SDL_Rect dest;
		dest.x = (mPosX + 0.5f - offsetX) * tileSizeX - mes->getWidth()/2;
		dest.y = (mPosY - offsetY - 1) * tileSizeY + mes->getHeight()/2;
		dest.h = mes->getHeight();
		dest.w = mes->getWidth();
		mes->render(NULL, &dest);
		delete mes;
	}
}

void Dynamic_Sign::update(float elapsedTime, Dynamic* player)
{
	if(mDrawMessage)
	{
		mTimeLeftToShow -= elapsedTime;
		if(mTimeLeftToShow <= 0)
			mDrawMessage = false;
	}
}

void Dynamic_Sign::onInteraction(Dynamic* player)
{
	mDrawMessage = true;
	mTimeLeftToShow = sSHOW_TIME;
}
