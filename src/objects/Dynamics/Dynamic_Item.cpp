#include "objects/Dynamics/Dynamic_Item.hpp"

Dynamic_Item::Dynamic_Item(float x, float y, Item* item) : Dynamic("pickup")
{
	mPosX = x;
	mPosY = y;
	mSolidVsDyn = false;
	mSolidVsMap = false;
	mFriendly = true;

	mItem = item;

	mCollected = false;
}

void Dynamic_Item::draw(float offsetX, float offsetY, int tileSizeX, int tileSizeY)
{
	if(!mCollected)
	{
		SDL_Rect dest;
		dest.x = (mPosX - offsetX) * tileSizeX;
		dest.y = (mPosY - offsetY) * tileSizeY;
		dest.w = tileSizeX;
		dest.h = tileSizeY;
        mItem->getTexture()->render(NULL, &dest);
	}
}

void Dynamic_Item::onInteraction(Dynamic* player)
{
	if(!mCollected)
	{
		if(mItem->onInteract(player))
		{
			//Add the item the the player's inventory
			Creature_Player* p = (Creature_Player*)player;
			p->getInventory()->giveItem(mItem);
		}

		mCollected = true;
	}
}
