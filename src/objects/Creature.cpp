#include "objects/Creature.h"

//Define static constants
const float Creature::sTIME_TO_SHOW = 2.5f;

Creature::Creature(std::string name, Texture &texture) : Dynamic(name)
{
	mTexture = texture;

	mHealthBar = Assets::getInstance().getTexture(Textures::TEX_BAR_HEALTH);
	mTimeBarShown = 0.0f;
	mShowBar = false;

	mHealth = 6;
	mHealthMax = 6;
	mAttackDmg = 1;

	mFighting = false;
	mAnimFightDelay = 0.0f;

	mFacingDirection = SOUTH;
	mState = STANDING;

	mTimer = 0.0f;
	mGraphicCounter = 0;

	mStateTick = 0.0f; //make a decision every frame
}

void Creature::update(float elapsedTime, Dynamic* player)
{
	const float LAPS_FRAMES = 0.2f;
	//Increase timer
	mTimer += elapsedTime;
	if(mTimer >= LAPS_FRAMES)
	{
		mTimer -= LAPS_FRAMES;
		mGraphicCounter++;
		mGraphicCounter %= 2;
	}

	//Chose the state
	if(mHealth <= 0)
		mState = DEAD;
	else if(mFighting)
	{
		mAnimFightDelay += elapsedTime;
		if(mAnimFightDelay < LAPS_FRAMES)
			mState = FIGHTING;
		else
			mFighting = false;
	}
	else if(mVelX != 0 || mVelY != 0)
		mState = WALKING;
	else
		mState = STANDING;

	//Determine direction
	if(mVelX < 0) mFacingDirection = WEST;
	if(mVelX > 0) mFacingDirection = EAST;
	if(mVelY < 0) mFacingDirection = NORTH;
	if(mVelY > 0) mFacingDirection = SOUTH;

	//Choose to draw health bar
	if(mShowBar)
	{
		mTimeBarShown += elapsedTime;
		if(mTimeBarShown >= sTIME_TO_SHOW)
		{
			mShowBar = false;
			mTimeBarShown = 0.0f;
		}
	}

	if(isAlive())
	{
		mSolidVsDyn = true;
		behaviour(elapsedTime, dynamic_cast<Creature*>(player));
	}
	else
	{
		mSolidVsDyn = false;
		stopMoving();
	}
}

void Creature::draw(float offsetX, float offsetY, int tileSizeX, int tileSizeY)
{
	//Generic positions in pixels depending on the texture dimensions
	const int TILE_SIZE_X = mTexture.getWidth() / 4;
	const int TILE_SIZE_Y = mTexture.getHeight() / 2;

	//Position on the texture to render
	int x = 0;
	int y = 0;

	switch(mState)
	{
	case STANDING:
		x = 1 * TILE_SIZE_X;
		y = 0;
		break;
	case WALKING:
		x = 1 * TILE_SIZE_X;
		y = mGraphicCounter * TILE_SIZE_Y;
		if(mVelX > 0 || mFacingDirection == EAST)
			mTexture.flip(SDL_FLIP_NONE);
		if(mVelX < 0 || mFacingDirection == WEST)
			mTexture.flip(SDL_FLIP_HORIZONTAL);
		break;
	case DEAD:
		x = 2 * TILE_SIZE_X;
		y = 0;
		break;
	case FIGHTING:
        x = 2 * TILE_SIZE_X;
        y = 1 * TILE_SIZE_Y;
		break;
	}

	//Draw the creature
	SDL_Rect src, dest;
	src.x = x;
	src.y = y;
	src.w = TILE_SIZE_X;
	src.h = TILE_SIZE_Y;
	dest.x = (mPosX - offsetX) * tileSizeX;
	dest.y = (mPosY - offsetY) * tileSizeY;
	dest.w = tileSizeX;
	dest.h = tileSizeY;
	mTexture.render(&src, &dest);

	//Draw the health bar
	if(mShowBar)
	{
		const int HEALTH_BAR_WIDTH = mHealthBar->getWidth();
		const int HEALTH_BAR_HEIGHT = mHealthBar->getHeight() / FRAMES_BAR;
		//TODO: fix when health is near 0 but not dead (display is wrong)
		SDL_Rect srcBar = {0, (int)(((float)mHealth/(float)mHealthMax) * (FRAMES_BAR - 1)) * HEALTH_BAR_HEIGHT, HEALTH_BAR_WIDTH, HEALTH_BAR_HEIGHT},
		destBar = {(int)((mPosX - offsetX) * tileSizeX + tileSizeX * 0.5f - HEALTH_BAR_WIDTH), dest.y - HEALTH_BAR_HEIGHT * 2, HEALTH_BAR_WIDTH * 2, HEALTH_BAR_HEIGHT * 2};
		mHealthBar->render(&srcBar, &destBar);
	}
}

void Creature::behaviour(float elapedTime, Creature* player)
{
	//No default behaviour
}

void Creature::defineCharacteristics(int maxHealth, int attackDmg)
{
	mHealthMax = maxHealth;
	mAttackDmg = attackDmg;
}

void Creature::changeMaxHealth(int maxHealth)
{
	mHealthMax = std::max(1, maxHealth);
}

void Creature::attack(Creature* ennemy)
{
	//Have to wait until delay for new fight is passed
	if(!mFighting)
	{
		mFighting = true;
		mAnimFightDelay = 0.0f;
		ennemy->takeDamage(mAttackDmg);
	}
}

void Creature::takeDamage(int dmg)
{
	//Normalize dmg and Loose health
	mHealth -= (dmg < 0 ? -dmg : dmg);

	//Normalize health
	mHealth = std::max(0, mHealth);

	showHealthBar();
}

void Creature::heal(int restored)
{
	//Normalize restored and gain health
	mHealth += restored < 0 ? -restored : restored;

	//Normalize health
	mHealth = std::min(mHealthMax, mHealth);

	showHealthBar();
}

void Creature::repop(float x, float y)
{
	mHealth = mHealthMax;
	mPosX = x;
	mPosY = y;
}

bool Creature::isAlive()
{
	return mHealth > 0;
}

Creature::Directions Creature::getFacingDirection()
{
	return mFacingDirection;
}

void Creature::showHealthBar()
{
    mShowBar = true;
    mTimeBarShown = 0.0f;
}

int Creature::getHealthMax() const
{
	return mHealthMax;
}

int Creature::getHelath() const
{
	return mHealth;
}
