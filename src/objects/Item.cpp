#include "objects/Item.hpp"

Item::Item(std::string name, Texture& texture, std::string description)
{
	mName = name;
	mTexture = texture;
	mDescription = description;

	mKeyItem = false;
	mEquipable = false;
}

std::string Item::getName() const
{
	return mName;
}

std::string Item::getDescription() const
{
	return mDescription;
}

Texture* Item::getTexture()
{
	return &mTexture;
}

bool Item::isKeyItem() const
{
	return mKeyItem;
}

bool Item::isEquipable() const
{
	return mEquipable;
}
