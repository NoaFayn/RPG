#include "objects/Items/Item_Key.hpp"

Item_Key::Item_Key() : Item("Key", *Assets::getInstance().getTexture(Textures::TEX_ITEM_KEY), "Used to unlock doors")
{
	mKeyItem = true;
}

bool Item_Key::onInteract(Dynamic* object)
{
	return true;
}

bool Item_Key::onUse(Dynamic* object)
{
	return true;
}
