#include "objects/Items/Item_HealthBoost.hpp"

#include "usefullPack/Assets.h"

Item_HealthBoost::Item_HealthBoost() : Item("Health Boost", *Assets::getInstance().getTexture(Textures::TEX_ITEM_HEALTH_BOOST), "Increases max health by 6")
{

}

bool Item_HealthBoost::onInteract(Dynamic* object)
{
	return true; //TRUE: add to the player's inventory
}

bool Item_HealthBoost::onUse(Dynamic* object)
{
	if(object != nullptr)
	{
		Creature* dyn = (Creature*)object;
		dyn->changeMaxHealth(dyn->getHealthMax() + 6);
		dyn->heal(6);
	}

	return true; //TRUE: remove from inventory
}
