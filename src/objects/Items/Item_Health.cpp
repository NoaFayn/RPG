#include "objects/Items/Item_Health.hpp"

#include "usefullPack/Assets.h"

Item_Health::Item_Health() : Item("Health", *Assets::getInstance().getTexture(Textures::TEX_ITEM_HEALTH), "Restores 5 health")
{

}

bool Item_Health::onUse(Dynamic* object)
{
	if(object != nullptr)
	{
		Creature* dyn = (Creature*)object;
		dyn->heal(5);
	}

	return true; //TRUE: the item is consumed and have to disappear
}

bool Item_Health::onInteract(Dynamic* object)
{
	onUse(object);
	return false; //FALSE: do not add the item to the player's inventory
}
