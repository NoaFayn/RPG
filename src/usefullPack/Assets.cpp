#include "usefullPack/Assets.h"

#include "rpg/Map.h"
#include "objects/Items/Item_Health.hpp"
#include "objects/Items/Item_HealthBoost.hpp"
#include "objects/Items/Item_Key.hpp"

using namespace std;

Assets::Assets(){}
Assets::~Assets(){}

Assets& Assets::getInstance()
{
	static Assets me;
	return me;
}

bool Assets::loadTextures()
{
	// [[[ TEXTURES ]]]
	// === ENTITIES ===
	//Player texture
    if(!loadTexture(TEX_DYN_PLAYER, "assets/player.png"))
	{
		cout << "Unable to load TEX_DYN_PLAYER!" << endl;
		return false;
	}
	//Ghost
	if(!loadTexture(TEX_DYN_GHOST, "assets/ghost.png"))
	{
		cout << "Unable to load TEX_DYN_GHOST!" << endl;
		return false;
	}
	//Skeleton texture
	if(!loadTexture(TEX_DYN_SKELETON, "assets/skeleton.png"))
	{
		cout << "Unable to load TEX_DYN_SKELETON!" << endl;
		return false;
	}



	// === MENUS ===
	if(!loadTexture(TEX_MEN_MAIN, "assets/menu.png"))
	{
		cout << "Unable to load MENU_MAIN!" << endl;
		return false;
	}

	// === MAPS ===
	//Village 1
	if(!loadTexture(TEX_MAP_VILLAGE1, "assets/map_village1.png"))
	{
		cout << "Unable to load TEX_MAP_VILLAGE1!" << endl;
		return false;
	}
	//Labo 1
	if(!loadTexture(TEX_MAP_GLOBAL, "assets/map_global.png"))
	{
		cout << "Unable to load TEX_MAP_GLOBAL!" << endl;
		return false;
	}

	// === LOOTS ===
	if(!loadTexture(TEX_LOOT_SWORD, "assets/loot_sword.png"))
	{
		std::cout << "Unable to load TEX_LOOT_SWORD!" << std::endl;
		return false;
	}

	// === BARS ===
	if(!loadTexture(TEX_BAR_HEALTH, "assets/bar_health.png"))
	{
		std::cout << "Unable to load TEX_BAR_HEALTH!" << std::endl;
		return false;
	}

	// === ITEMS ===
	//Health
	if(!loadTexture(TEX_ITEM_HEALTH, "assets/item_health.png"))
	{
		std::cout << "Unable to load TEX_ITEM_HEALTH!" << std::endl;
		return false;
	}
	//Health boost
	if(!loadTexture(TEX_ITEM_HEALTH_BOOST, "assets/item_health_boost.png"))
	{
		std::cout << "Unable to load TEX_ITEM_HEALTH_BOOST!" << std::endl;
		return false;
	}
	//Key
	if(!loadTexture(TEX_ITEM_KEY, "assets/item_key.png"))
	{
		std::cout << "Unable to load TEX_ITEM_KEY!" << std::endl;
		return false;
	}
	//Sign
	if(!loadTexture(TEX_DYN_SIGN, "assets/item_sign.png"))
	{
		std::cout << "Unable to load TEX_ITEM_SIGN!" << std::endl;
		return false;
	}

	return true;
}

bool Assets::loadMaps()
{
	// [[[ MAPS ]]]
	loadMap(new map_Village1());
	loadMap(new map_Labo1());

	return true;
}

bool Assets::loadFonts()
{
	// [[[ FONTS ]]]
	if(!loadFont(FNT_RPG, "assets/rpgFont.ttf"))
	{
		std::cout << "Unable to load FNT_RPG!" << std::endl;
		return false;
	}
	if(!loadFont(FNT_GLOBAL, "assets/gil.ttf"))
	{
		std::cout << "Unable to load FNT_GLOBAL!" << std::endl;
		return false;
	}

	return true;
}

bool Assets::loadItems()
{
	// [[[ ITEMS ]]]
	// === HEALTH ===
	loadItem(new Item_Health());
	loadItem(new Item_HealthBoost());
	loadItem(new Item_Key());

	return true;
}

Texture* Assets::getTexture(Textures textureId)
{
	return mTextures[textureId];
}

Map* Assets::getMap(std::string mapName)
{
	return mMaps[mapName];
}

Font* Assets::getFont(FontsId fontId)
{
	return mFonts[fontId];
}

Item* Assets::getItem(std::string itemName)
{
	return mItems[itemName];
}

bool Assets::loadTexture(Textures id, std::string path)
{
	//Create texture
	Texture* texture = new Texture();
	if(!texture->loadFromFile(path))
	{
		cout << "Unable to load texture " << path << "!" << endl;
		return false;
	}

	//Load texture
	mTextures[id] = texture;
	return true;
}

void Assets::loadMap(Map* m)
{
	//Load map
	mMaps[m->getName()] = m;
}

bool Assets::loadFont(FontsId id, std::string path)
{
    //Create ttf_font
    TTF_Font* ttfFont = TTF_OpenFont(path.c_str(), 20);
    if(ttfFont == nullptr)
	{
		std::cout << "Unable to create TTF FONT " << path << "!" << std::endl;
		return false;
	}
	//Create font
	Font* font = new Font(ttfFont);

	//Load font
	mFonts[id] = font;
	return true;
}

void Assets::loadItem(Item* i)
{
	//Load item
	mItems[i->getName()] = i;
}
