#include "usefullPack/TextureManager.h"

using namespace std;

Texture::Texture()
{
	mTexture = NULL;

	mWidth = mHeight = 0;

	mAngle = 0;
	mCenter = new SDL_Point();
	mCenter->x = mCenter->y = 0;
	mFlip = SDL_FLIP_NONE;
}

Texture::~Texture()
{
	//Deallocate memory
	free();
}

bool Texture::initialize(SDL_Window* window, SDL_Renderer* renderer)
{
	if(window == NULL)
	{
		cout << "The WINDOW initialized is NULL!" << endl;
		return false;
	}
	Texture::gWindow = window;

	if(renderer == NULL)
	{
		cout << "The RENDERER initialized is NULL!" << endl;
		return false;
	}
	Texture::gRenderer = renderer;

	return true;
}

bool Texture::loadFromFile(std::string path)
{
	if(gWindow == NULL || gRenderer == NULL)
	{
		cout << "Need to initialize TEXTURE CLASS before using it" << endl;
		return false;
	}

	//Deallocate previous texture
	free();

	//Final texture
	SDL_Texture* newTexture = NULL;

	//Load image at specified path
	SDL_Surface* loadedSurface = IMG_Load(path.c_str());
	if(loadedSurface == NULL)
	{
		cout << "Unable to load image at " << path << "!\nERROR: " << IMG_GetError() << endl;
		return false;
	}

	//Transparency
//SDL_SetColorKey(loadedSurface, SDL_TRUE, SDL_MapRGB(loadedSurface->format, 0xFF, 0xFF, 0xFF));

	//Create texture from surface pixels
	newTexture = SDL_CreateTextureFromSurface(gRenderer, loadedSurface);
	if(newTexture == NULL)
	{
		cout << "Unable to create texture from " << path << "!\nERROR: " << SDL_GetError() << endl;
		return false;
	}

	//Get image dimensions
	mWidth = loadedSurface->w;
	mHeight = loadedSurface->h;

	//Deallocate old surface
	SDL_FreeSurface(loadedSurface);

	//Save the texture
	mTexture = newTexture;
	return mTexture != NULL;
}

bool Texture::loadFromRenderedText(std::string textureText, Font* font)
{
	if(gWindow == NULL || gRenderer == NULL)
	{
		cout << "Need to initialize TEXTURE CLASS before using it" << endl;
		return false;
	}

	//Destroy the old texture
    free();

    //Render text surface
    SDL_Surface* textSurface = TTF_RenderText_Solid(font->getFont(), textureText.c_str(), font->getColor());
    if(textSurface == NULL)
    {
        printf("Unable to render text surface!\nTTF error: %s\n", TTF_GetError());
    }
    else
    {
        //Create texture from surface pixels
        mTexture = SDL_CreateTextureFromSurface(gRenderer, textSurface);
        if(mTexture == NULL)
        {
            printf("Unable to create texture from rendered text!\nerror: %s\n", SDL_GetError());
        }
        else
        {
            //Get image dimensions
            mWidth = textSurface->w;
            mHeight = textSurface->h;
        }

        //Destroy the old surface
        SDL_FreeSurface(textSurface);
    }

    //Return success
    return mTexture != NULL;
}

void Texture::free()
{
	//Deallocate texture if it exists
	if(mTexture != NULL)
	{
		SDL_DestroyTexture(mTexture);
		mTexture = NULL;
		mWidth = mHeight = 0;
	}

}

void Texture::setColor(Uint8 red, Uint8 green, Uint8 blue)
{
	if(gWindow == NULL || gRenderer == NULL)
	{
		cout << "Need to initialize TEXTURE CLASS before using it" << endl;
		return;
	}

	//Modulate texture rgb
	SDL_SetTextureColorMod(mTexture, red, green, blue);
}

void Texture::setBlendMode(SDL_BlendMode blending)
{
	if(gWindow == NULL || gRenderer == NULL)
	{
		cout << "Need to initialize TEXTURE CLASS before using it" << endl;
		return;
	}

	//Set blending function
	SDL_SetTextureBlendMode(mTexture, blending);
}

void Texture::setAlpha(Uint8 alpha)
{
	if(gWindow == NULL || gRenderer == NULL)
	{
		cout << "Need to initialize TEXTURE CLASS before using it" << endl;
		return;
	}

	//Modulate texture alpha
	SDL_SetTextureAlphaMod(mTexture, alpha);
}

void Texture::render(SDL_Rect* src, SDL_Rect* dest)
{
	if(gWindow == NULL || gRenderer == NULL)
	{
		cout << "Need to initialize TEXTURE CLASS before using it" << endl;
		return;
	}

	if(src != NULL && dest != NULL)
	{
		//Render to screen
		SDL_RenderCopyEx(gRenderer, mTexture, src, dest, mAngle, mCenter, mFlip);
	}
	else if(src == NULL && dest != NULL)
	{
		//The source is the all texture
		SDL_Rect src = {0, 0, mWidth, mHeight};
		SDL_RenderCopyEx(gRenderer, mTexture, &src, dest, mAngle, mCenter, mFlip);
	}
	else
	{
		//The source is all the texture
		//The destination is like the texture
		SDL_Rect src = {0, 0, mWidth, mHeight};
		SDL_Rect dest = src;
		SDL_RenderCopyEx(gRenderer, mTexture, &src, &dest, mAngle, mCenter, mFlip);
	}

}

int Texture::getWidth()
{
	return mWidth;
}

int Texture::getHeight()
{
	return mHeight;
}

void Texture::rotation(double angle)
{
	mAngle = angle;
}

void Texture::changeCenter(SDL_Point* center)
{
	mCenter = center;
}

void Texture::flip(SDL_RendererFlip flip)
{
	mFlip = flip;
}




// ====================
// | STATIC VARIABLES |
// ====================
SDL_Window* Texture::gWindow = NULL;
SDL_Renderer* Texture::gRenderer = NULL;

// ================
// | DRAW METHODS |
// ================
void draw_fillRect(SDL_Rect fillRect, SDL_Color color)
{
	SDL_SetRenderDrawColor(Texture::gRenderer, color.r, color.g, color.b, color.a);
	SDL_RenderFillRect(Texture::gRenderer, &fillRect);
}

void draw_lineRect(SDL_Rect fillRect, SDL_Color color)
{
	int x1 = fillRect.x;
	int y1 = fillRect.y;
	int x2 = fillRect.x + fillRect.w;
	int y2 = y1 + fillRect.h;
	draw_line(x1, y1, x2, y1, 1, color);
	draw_line(x2, y1, x2, y2, 1, color);
	draw_line(x2, y2, x1, y2, 1, color);
	draw_line(x1, y2, x1, y1, 1, color);
}

void draw_line(int x1, int y1, int x2, int y2, int w, SDL_Color color)
{
	SDL_SetRenderDrawColor(Texture::gRenderer, color.r, color.g, color.b, color.a);
	SDL_RenderDrawLine(Texture::gRenderer, x1, y1, x2, y2);
}

void draw_text(std::string text, int x, int y, Font &f)
{
	Texture* tex = new Texture();
	tex->loadFromRenderedText(text, &f);
	SDL_Rect dest;
	dest.x = x;
	dest.y = y;
	dest.w = tex->getWidth();
	dest.h = tex->getHeight();
	tex->render(NULL, &dest);
	delete tex;
}
