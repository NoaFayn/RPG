#include "usefullPack/Menu.h"

Menu::Menu(Texture* background)
{
	mBackground = background;
	mTitle = new Texture();
}

Menu::~Menu()
{
    delete mBackground;

	delete mTitle;

	for(auto *t : mChoices)
		delete t;
    mChoices.clear();
}

void Menu::addChoices(std::vector<std::string> choices, Font* font)
{
	for(unsigned int i = 0; i < choices.size(); i++)
	{
		//Create texture
		Texture tex;
		tex.loadFromRenderedText(choices[i], font);
		//Add texture on the array
		mChoices.push_back(&tex);
	}
}

void Menu::addChoice(std::string choice, Font* font)
{
	//Create texture
	Texture tex;
	tex.loadFromRenderedText(choice, font);
	//Add texture on the array
	mChoices.push_back(&tex);
}

void Menu::setTitle(std::string title, Font* font)
{
	//Create texture
	Texture tex;
	tex.loadFromRenderedText(title, font);
	//Set title
	mTitle = &tex;
}

void Menu::render()
{
	//Background
	mBackground->render(NULL, NULL);
	//Title
	mTitle->render(NULL, NULL);
	//Choices
	for(unsigned int i = 0; i < mChoices.size(); i++)
	{
		std::cout << i << std::endl;
		SDL_Rect dest;
		dest.x = 0;
		dest.y = i * 30;
		dest.w = mChoices[i]->getWidth();
		dest.h = mChoices[i]->getHeight();
		mChoices[i]->render(NULL, &dest);
	}
}
