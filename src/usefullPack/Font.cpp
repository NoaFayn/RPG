#include "usefullPack/Font.h"

//Define static variables
SDL_Color Font::sColorBlack = {0x00, 0x00, 0x00, 0xFF};
SDL_Color Font::sColorRed = {0xFF, 0x00, 0x00, 0xFF};
SDL_Color Font::sColorWhite = {0xFF, 0xFF, 0xFF, 0xFF};

Font::Font(TTF_Font* font)
{
	mFont = font;
	mColor = sColorBlack;
}

Font::~Font()
{
	TTF_CloseFont(mFont);
}

void Font::changeColor(SDL_Color color)
{
	mColor = color;
}

TTF_Font* Font::getFont() const
{
	return mFont;
}

SDL_Color Font::getColor() const
{
	return mColor;
}
